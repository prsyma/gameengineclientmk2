#ifndef RENDER_SYSTEM
#define RENDER_SYSTEM
#include "ECS.h"
#include "Components.h"
#include "PoolAllocator.h"
#include "DrawList.h"
#include "Messenger.h"
#include "PoolMessageAllocator.h"
#include "MessageTypes.h"
#include "glm.hpp"
#pragma once
class RenderSystem : public ECS::System
{
public:
	void Register(ECS::World* world) override;

	void Update(ECS::World* world) override;

	void Unregister(ECS::World* world) override;

	void GetDrawList();

	~RenderSystem() override;
private:
	ECS::Entity* player;
	DrawList* drawList;
	static Messenger* m;
#undef MessageBox
	MessageBox* box;
#define MessageBox MessageBoxW
};

#endif