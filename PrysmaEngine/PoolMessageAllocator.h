
#include "MemoryManager.h"

#pragma once
template<typename T>
/// <summary>
/// Class representing the allocator for the message pool
/// </summary>
class PoolMessageAllocator
{
private:
	size_t OBJECT_SIZE;
public:
	/// <summary>
	/// constructor
	/// </summary>
	/// <param name="memSize">size of emmeory</param>
	/// <param name="mem">pointer to memeory</param>
	/// <param name="objectSize">size of object</param>
	PoolMessageAllocator(size_t memSize, const void* mem, size_t objectSize);
	typedef T value_type;
	typedef value_type* pointer;
	typedef const value_type* const_pointer;
	typedef value_type& reference;
	typedef const value_type& const_reference;
	typedef std::size_t size_type;
	typedef std::ptrdiff_t difference_type;
	virtual ~PoolMessageAllocator();
	pointer address(reference value) const {
		return &value;
	}
	const_pointer address(const_reference value) const {
		return &value;
	}
	/// <summary>
	/// allocates
	/// </summary>
	/// <param name="size">size of ememroy required</param>
	/// <returns>pointer</returns>
	virtual pointer allocate(size_t size)
	{
		MemoryManager* m = &MemoryManager::getInstance();

		return (pointer)m->allocatePool(size * sizeof(T));
	};
	virtual void deallocate(void* p, size_type num);

	template<typename U>
	struct rebind {
		typedef PoolMessageAllocator<U> other;
	};

	PoolMessageAllocator() throw() {};
	template<typename U>
	PoolMessageAllocator(const PoolMessageAllocator<U>&) throw() {};
	template< class U, class... Args >
	void construct(U* p, Args&& ... args);

};

template<typename T>
inline PoolMessageAllocator<T>::PoolMessageAllocator(size_t memSize, const void* mem, size_t objectSize) : OBJECT_SIZE(objectSize)
{
}

template<typename T>
inline PoolMessageAllocator<T>::~PoolMessageAllocator()
{
}



template<typename T>
/// <summary>
/// function to deallocate
/// </summary>
/// <param name="p">pointer to deallocate</param>
/// <param name="num">amount to deallocate</param>
inline void PoolMessageAllocator<T>::deallocate(void* p, size_type num)
{

	MemoryManager* m = &MemoryManager::getInstance();

	m->freePool(p);
}





;

template<typename T>
template<class U, class ...Args>
/// <summary>
/// function to cosntruct the obejct
/// </summary>
/// <param name="p">pointer to construct</param>
/// <param name="...args">variadic arguments</param>
inline void PoolMessageAllocator<T>::construct(U* p, Args&& ...args)
{
	new((void*)p) U(std::forward<Args>(args)...);
}
