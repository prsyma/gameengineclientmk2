#include "MessageBox.h"

MessageBox::MessageBox(int id)
{
	this->id = id;
	normal = MessageList();
	instant = MessageList();
	
}

void MessageBox::RecieveNormal(Message message)
{
	normal.StoreMessage(message);
}

void MessageBox::RecieveInstant(Message message)
{
	instantMessage = true;
	instant.StoreMessage(message);
}

bool MessageBox::NotEmpty()
{
	return !normal.Empty();
}

Message MessageBox::pop()
{
	return normal.Pop();
}

Message MessageBox::popInstant()
{
	if (instant.Count() == 1)
	{
		instantMessage = false;
	}
	return instant.Pop();
}
