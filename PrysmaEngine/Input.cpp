#include "Input.h"

bool Input::Get(int keycode)
{
	auto i = KeyMap.find(keycode);
	if (i != KeyMap.end()) 
	{
		//pressed
		if (KeyMap.at(keycode) == 1 || KeyMap.at(keycode) == 2)
		{
			return true;
		}
		return false;
	}
	return false;
}

void Input::Set(int keycode, int state)
{
	auto i = KeyMap.find(keycode);
	if (i != KeyMap.end())
	{
		//we can update
		if (state == 0) 
		{
			//we can remove
			KeyMap.erase(keycode);
		}
		else 
		{
			KeyMap.insert_or_assign(keycode, state);
		}
	}
	else 
	{
		if (state == 1 || state == 2) 
		{
			KeyMap.insert_or_assign(keycode, state);
		}
	}
	return;
}

MouseLocation* Input::GetMouseLocation()
{
	return ml;
}

void Input::SetMouseLocation(double x, double y)
{
	ml->x = x;
	ml->y = y;
}

bool Input::GetMouseButton(int buttonCode)
{
	auto i = MouseMap.find(buttonCode);
	if (i != MouseMap.end())
	{
		//pressed
		if (MouseMap.at(buttonCode) == 1 || MouseMap.at(buttonCode) == 2)
		{
			return true;
		}
		return false;
	}
	return false;
}

void Input::SetMouseButton(int code, int state)
{
	auto i = MouseMap.find(code);
	if (i != MouseMap.end())
	{
		//we can update
		if (state == 1)
		{
			//we can remove
			MouseMap.erase(code);
		}
		else
		{
			MouseMap.insert_or_assign(code, state);
		}
	}
	else
	{
		if (state == 0)
		{
			MouseMap.insert_or_assign(code, state);
		}
	}
	return;
}

Input::Input()
{
	StackAllocator<MouseLocation> alloc;
	ml = alloc.allocate(1);
	alloc.construct(ml);
}
