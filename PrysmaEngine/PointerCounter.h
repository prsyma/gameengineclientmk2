#pragma once
/// ********************************************************************************
/// <summary>
/// counter for smart pointers
/// </summary>
/// ********************************************************************************
class PointerCounter
{
public:
	/// --------------------------------------------------------------------------------
	/// <summary>
	/// increments countrer
	/// </summary>
	/// <created>Reece,08/06/2019</created>
	/// <changed>Reece,08/06/2019</changed>
	/// --------------------------------------------------------------------------------
	void increment();
	/// --------------------------------------------------------------------------------
	/// <summary>
	/// decrements counter
	/// </summary>
	/// <returns></returns>
	/// <created>Reece,08/06/2019</created>
	/// <changed>Reece,08/06/2019</changed>
	/// --------------------------------------------------------------------------------
	int decrement();
private:

	/// <summary>actual count</summary>
	int count = 0;
};

