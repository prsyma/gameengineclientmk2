#include <unordered_map>
#include "PoolAllocator.h"
#include <string>
#include <iostream>
#include <fstream>
#include "ECS.h"
#include <vector>
#include <algorithm>
#include "Components.h"
#include "Glad/glad.h"

#pragma once
/// ********************************************************************************
/// <summary>
/// Class used to manager the graphics related objects
/// </summary>
/// ********************************************************************************

/// <summary>
/// mapping of basic string to use our pool allocator
/// </summary>
typedef std::basic_string<char, std::char_traits<char>, PoolAllocator<char>> cachedString;
class GraphicsHashSytem
{

public:
	/// <summary>
	/// load basic model
	/// </summary>
	/// <param name="vertShader">vert shader</param>
	/// <param name="fragShader">frag shader</param>
	/// <param name="verts">string to obj</param>
	/// <returns>GraphicItem</returns>
	GraphicItem Load(std::string vertShader, std::string fragShader, std::string verts);
	/// <summary>
	/// Load Terrain
	/// </summary>
	/// <param name="vertShader">vertShader</param>
	/// <param name="fragShader">FragSjader</param>
	/// <param name="terrain">terrain data</param>
	/// <param name="world">ECS world</param>
	/// <returns></returns>
	GraphicItem LoadTerrain(std::string vertShader, std::string fragShader, std::vector<glm::vec3, PoolAllocator<glm::vec3>> terrain, ECS::World* world);
	/// <summary>
	/// function to unload
	/// </summary>
	/// <param name="id"></param>
	void Unload(int id);
private:
	/// <summary>
	/// map of graphic items to ids
	/// </summary>
	static std::unordered_map<unsigned int, GraphicItem,std::hash<int>,std::equal_to<unsigned int>,PoolAllocator<std::pair<unsigned int, GraphicItem>>> map;
	/// <summary>
	/// load a shader program
	/// </summary>
	/// <param name="vertShader">vert shader</param>
	/// <param name="fragShader">frag shader</param>
	/// <returns>program id</returns>
	int loadProgram(std::string vertShader, std::string fragShader);
	int loadOBJ(std::string verts);
	/// <summary>
	/// function to read a shader
	/// </summary>
	/// <param name="fp"></param>
	/// <returns></returns>
	cachedString readShader(const char* fp);
	
};

