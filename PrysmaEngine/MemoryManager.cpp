#include "SmartPointer.h"
#include "MemoryManager.h"
Stack* MemoryManager::stack = &Stack::getInstance();
MessagePool* MemoryManager::messagePool = &MessagePool::getInstance();
Pool* MemoryManager::pool = &Pool::getInstance();
void* MemoryManager::memStart = nullptr;

void* MemoryManager::allocatePool(size_t size)
{
	return pool->allocate(size);
}

void MemoryManager::freePool(void* ptr)
{
	pool->free(ptr);
}

void* MemoryManager::allocateMessagePool(size_t size)
{
	return messagePool->allocate(size);
}

void MemoryManager::freeMessagePool(void* ptr)
{
	messagePool->free(ptr);
}

void* MemoryManager::allocateStack(size_t size)
{
	return stack->allocate(size);
}

void MemoryManager::freeStack(void* ptr)
{
	stack->deallocate(ptr);
}

void MemoryManager::DecrementCounter(MemoryType memType, int wrapperID)
{
	if (memType == MemoryType::POOL)
	{
		pool->DecrementCounter(wrapperID);
	}
	else if (memType == MemoryType::STACK)
	{
		stack->decrementCounter(wrapperID);
	}
	else if (memType == MemoryType::GRAPHICS)
	{

	}
}

void MemoryManager::IncrementCounter(MemoryType memType, int wrapperID)
{
	if (memType == MemoryType::POOL)
	{
		pool->IncrementCounter(wrapperID);
	}
	else if (memType == MemoryType::STACK)
	{
		stack->incrementCounter(wrapperID);
	}
	else if (memType == MemoryType::GRAPHICS)
	{

	}
}

std::string MemoryManager::readout()
{
	std::string tmp;
	tmp = "Total Memory: " + totalMem;
	tmp += "Memory Distribution:\nPool: " +pool->totalMem();
	tmp += "\nMessage Pool: " + messagePool->totalMem();
	tmp += "\nStack: " + stack->GetTotalMemory();
	tmp += "\nFree Memory:\nPool" + pool->GetFreeMem();
	tmp += "\nMessage Pool: " + messagePool->GetFreeMem();
	tmp+="\nStack: " + stack->memoryRemaining();
	return tmp;
}

MemoryManager::MemoryManager()
{
	if (!setup) 
	{
		setup = true;
		int sizeOfPool = 1073741824;
		int poolBlockSize = 1024;
		int stackSize = 53687091;
		memStart = std::malloc(sizeOfPool + stackSize + sizeOfPool);
		pool = &Pool::getInstance();
		messagePool = &MessagePool::getInstance();
		stack = &Stack::getInstance();
		if (!pool->IsSetUp()) 
		{
			pool->SetUp((char*)memStart + stackSize, sizeOfPool, poolBlockSize);
		}
		if (!messagePool->IsSetUp()) 
		{
			messagePool->SetUp((char*)memStart + stackSize + sizeOfPool, sizeOfPool, poolBlockSize);
		}
	
		if (!stack->IsSetUp())
		{
			stack->SetUp(memStart, (char*)memStart + stackSize, stackSize);
		}
	}
	
	


}

MemoryManager::~MemoryManager()
{
	bool b = true;
}
