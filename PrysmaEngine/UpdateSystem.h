#ifndef UPDATE_SYSTEM
#define UPDATE_SYSTEM
#pragma once
#include "ECS.h"
#include "Messenger.h"
#include "PoolMessageAllocator.h"
#include "MessageTypes.h"
#include "Components.h"
class UpdateSystem : public ECS::System
{
public:
	UpdateSystem();
	void Register(ECS::World* world) override;

	void Update(ECS::World* world) override;

	void Unregister(ECS::World* world) override;
	~UpdateSystem();
private:
	float timeToUpdate = 0.01f;
	float timeRemaining;
	static Messenger* m;
	#undef MessageBox
	MessageBox* box;
	#define MessageBox MessageBoxW
	PoolMessageAllocator<PositionUpdate> alloc;
};

#endif