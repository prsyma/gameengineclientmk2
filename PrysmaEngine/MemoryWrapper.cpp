#include "MemoryWrapper.h"

MemoryWrapper::MemoryWrapper(const MemoryWrapper& wrapper)
{
	this->counter = wrapper.counter;
	this->id = wrapper.id;
	this->location = wrapper.location;
	this->markedForDealloc = wrapper.markedForDealloc;
	this->size = wrapper.size;
	
}

void MemoryWrapper::operator=(MemoryWrapper const& wrapper)
{

	this->counter = wrapper.counter;
	this->id = wrapper.id;
	this->location = wrapper.location;
	this->markedForDealloc = wrapper.markedForDealloc;
	this->size = wrapper.size;
}

MemoryWrapper::MemoryWrapper(int id)
{
	this->id = id;
	location = nullptr;
	counter = PointerCounter();
	size = 0;
}

void MemoryWrapper::updateLocation(void* loc)
{
	location = loc;
}

void* MemoryWrapper::getLocation()
{
	return location;
}

bool MemoryWrapper::Used()
{
	if (location == nullptr) 
	{
		return false;
	}
	return true;
}
