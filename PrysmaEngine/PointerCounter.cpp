#include "PointerCounter.h"

void PointerCounter::increment()
{
	count++;
}

int PointerCounter::decrement()
{
	count--;
	return count;
}
