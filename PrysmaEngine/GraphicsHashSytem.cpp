#include "GraphicsHashSytem.h"
#define TINYOBJLOADER_IMPLEMENTATION
#include "tinyobj/tiny_obj_loader.h"
std::unordered_map<unsigned int, GraphicItem, std::hash<int>, std::equal_to<unsigned int>, PoolAllocator<std::pair<unsigned int, GraphicItem>>> GraphicsHashSytem::map = std::unordered_map<unsigned int, GraphicItem, std::hash<int>, std::equal_to<unsigned int>, PoolAllocator<std::pair<unsigned int, GraphicItem>>>();
GraphicItem GraphicsHashSytem::Load(std::string vertShader, std::string fragShader, std::string verts)
{
	GLuint VAO = 0;
	GLuint data = 0;
	//load obj
	PoolAllocator<tinyobj::attrib_t> alloc;
	tinyobj::attrib_t* atrribs = alloc.allocate(1);
	alloc.construct(atrribs);
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string warn;
	std::string err;
	bool ret = tinyobj::LoadObj(atrribs, &shapes, &materials, &warn, &err, verts.c_str());
	//create vbo

	if (!warn.empty()) {
		std::cout << "WARN: " << warn << std::endl;
	}

	if (!err.empty()) {
		std::cerr << "ERR: " << err << std::endl;
	}

	if (!ret) {
		printf("Failed to load/parse .obj.\n");
		return GraphicItem();
	}
	// Create the VAO
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glGenBuffers(1, &data);

	glBindBuffer(GL_ARRAY_BUFFER, data);
	glBufferData(GL_ARRAY_BUFFER, sizeof(atrribs->vertices[0]) * atrribs->vertices.size(), &atrribs->vertices[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);




	glBindVertexArray(0);
	//load program
	int progID = loadProgram(vertShader, fragShader);
	GraphicItem gi;
	gi.program = progID;
	gi.VAO = VAO;
	gi.vertLength = atrribs->vertices.size();

	
	return gi;
}

GraphicItem GraphicsHashSytem::LoadTerrain(std::string vertShader, std::string fragShader, std::vector<glm::vec3, PoolAllocator<glm::vec3>> terrain , ECS::World* world)
{
	GLuint VAO = 0;
	GLuint data = 0;
	


	// Create the VAO
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glGenBuffers(1, &data);

	glBindBuffer(GL_ARRAY_BUFFER, data);
	glBufferData(GL_ARRAY_BUFFER, sizeof(terrain[0]) * terrain.size(), &terrain[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);




	glBindVertexArray(0);
	//load program
	int progID = loadProgram(vertShader, fragShader);
	GraphicItem gi;
	gi.program = progID;
	gi.VAO = VAO;
	gi.vertLength = terrain.size();


	return gi;
}

void GraphicsHashSytem::Unload(int id)
{
	GLint attribs = 0;
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &attribs);
	glBindVertexArray(id);
	
	glGetIntegerv(GL_CURRENT_PROGRAM, &id);
	glDeleteProgram(id);
	for (int counter = 0; counter < attribs; ++counter) {
		GLint vboId = 0;
		glGetVertexAttribiv(counter, GL_VERTEX_ATTRIB_ARRAY_BUFFER_BINDING, &vboId);
		if (vboId > 0)
		{
			glDeleteBuffers(1, (GLuint*)& vboId);
		}
	}
	//then delete VAO
	glBindVertexArray(0);
	GLuint* val = (GLuint*)&id;
	glDeleteVertexArrays(1, val);
	map.erase(id);

}

int GraphicsHashSytem::loadProgram(std::string vertex_path, std::string fragment_path)
{
	GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);

	// Read shaders
	cachedString vertShaderStr = readShader(vertex_path.c_str());
	cachedString fragShaderStr = readShader(fragment_path.c_str());
	const char* vertShaderSrc = vertShaderStr.c_str();
	const char* fragShaderSrc = fragShaderStr.c_str();

	GLint result = GL_FALSE;
	int logLength;

	// Compile vertex shader
	std::cout << "Compiling vertex shader." << std::endl;
	glShaderSource(vertShader, 1, &vertShaderSrc, NULL);
	glCompileShader(vertShader);

	// Check vertex shader
	glGetShaderiv(vertShader, GL_COMPILE_STATUS, &result);
	glGetShaderiv(vertShader, GL_INFO_LOG_LENGTH, &logLength);

	cachedString vertShaderError;
	vertShaderError.resize(logLength);
	glGetShaderInfoLog(vertShader, logLength, NULL, &vertShaderError[0]);
	std::cout << &vertShaderError[0] << std::endl;

	// Compile fragment shader
	std::cout << "Compiling fragment shader." << std::endl;
	glShaderSource(fragShader, 1, &fragShaderSrc, NULL);
	glCompileShader(fragShader);

	// Check fragment shader
	glGetShaderiv(fragShader, GL_COMPILE_STATUS, &result);
	glGetShaderiv(fragShader, GL_INFO_LOG_LENGTH, &logLength);
	cachedString fragShaderError;
	fragShaderError.resize(logLength);
	glGetShaderInfoLog(fragShader, logLength, NULL, &fragShaderError[0]);
	std::cout << &fragShaderError[0] << std::endl;

	std::cout << "Linking program" << std::endl;
	GLuint program = glCreateProgram();
	glAttachShader(program, vertShader);
	glAttachShader(program, fragShader);
	glLinkProgram(program);

	glGetProgramiv(program, GL_LINK_STATUS, &result);
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
	std::vector<char> programError((logLength > 1) ? logLength : 1);
	glGetProgramInfoLog(program, logLength, NULL, &programError[0]);
	std::cout << &programError[0] << std::endl;

	glDeleteShader(vertShader);
	glDeleteShader(fragShader);

	return program;
}



cachedString GraphicsHashSytem::readShader(const char* fp)
{
	cachedString content;
	std::ifstream fileStream(fp, std::ios::in);

	if (!fileStream.is_open()) {
		std::cerr << "Could not read file " << fp << ". File does not exist." << std::endl;
		return "";
	}

	cachedString line = "";
	while (!fileStream.eof()) {
		std::getline(fileStream, line);
		content.append(line + "\n");
	}

	fileStream.close();
	return content;
}
