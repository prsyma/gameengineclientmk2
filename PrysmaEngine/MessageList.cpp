#include "MessageList.h"



MessageList::MessageList()
{
	
	holders = std::vector<Message>();
}

void MessageList::StoreMessage(Message message)
{
	count++;
	
	holders.push_back(message);
	
	

}

int MessageList::Count()
{
	return count;
}

Message MessageList::Pop()
{
	if (holders.size() > 0) 
	{
		Message m = holders[0];
		holders.erase(holders.begin());
		count--;
		return m;
	
	}
	else 
	{
		return Message();
	}
	
}

bool MessageList::Empty()
{
	return count == 0;
}
