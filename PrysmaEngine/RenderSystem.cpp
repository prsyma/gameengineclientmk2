#include "RenderSystem.h"
Messenger* RenderSystem::m = &Messenger::getInstance();

void RenderSystem::Register(ECS::World* world)
{
	player = world->GetPlayer();
	PoolAllocator<DrawList> alloc;
	drawList = alloc.allocate(1);
	alloc.construct(drawList);
	
}

void RenderSystem::Update(ECS::World* world)
{
	//update the draw list
	std::vector<ECS::Entity*, PoolAllocator<ECS::Entity*>> ents = *world->Entities();
	drawList->items.clear();
	//we would have more complex things
	ECS::ComponentWrapper<Camera> cam = player->get<Camera>();
	ECS::ComponentWrapper<Transform> platerTrans = player->get<Transform>();
	cam->GenereateVP(player->get<Transform>().get());
	std::cout << "Forward: " << platerTrans->GetCamFront().x << " "<< platerTrans->GetCamFront().y << " "<< platerTrans->GetCamFront().z << std::endl ;
	std::cout << "Pos: " << platerTrans->postion.x << " " << platerTrans->postion.y << " " << platerTrans->postion.z << std::endl;
	std::cout << "rot " << platerTrans->pitchDegrees << " rot y " << platerTrans->yawDegrees << std::endl;
	glm::mat4x4 proj = cam->proj;
	glm::mat4x4 view = cam->view;
	for (int i =0; i < ents.size(); i++)
	{
		if (ents[i]->has<Render>()) 
		{

			GraphicItem gi = ents[i]->get<Render>()->gi;
			gi.MVP = ents[i]->get<Transform>()->GetMVP(proj, view);
			drawList->items.push_back(DrawListItem(gi));
		}
	}
	drawList->numOfItems = drawList->items.size();
	//we need to send a message
	Message mes = Message();
	mes.data = (void*)drawList;
	mes.from = SYSTEMS::SCENE;
	mes.to =  SYSTEMS::GRAPHIC;
	mes.type = MessageType::DRAWLIST;
	m->Send(SYSTEMS::GRAPHIC, mes);
}

void RenderSystem::Unregister(ECS::World* world)
{
}

RenderSystem::~RenderSystem()
{
}
