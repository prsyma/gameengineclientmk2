#include "MessageTypes.h"
#include "MemoryManager.h"
#include "MessageBox.h"
#include "PoolMessageAllocator.h"
#include <vector>
#include "enums.h"
#pragma once
/// <summary>
/// class representing the messenger vack bone
/// </summary>
class Messenger
{
public:
	//meyers singleton 
	static Messenger& getInstance() 
	{
		static Messenger m;
		return m;
	}
	/// <summary>
	/// function to send a message
	/// </summary>
	/// <param name="dest">destination box</param>
	/// <param name="m">message tos end</param>
	void Send(int dest, Message m);
	//add in mutex for instant so it cannot be missed
	void Send_Instant(int dest, Message m);
	/// <summary>
	/// send message to all boxes
	/// </summary>
	/// <param name="m"></param>
	void Broadcast(Message m);
	/// <summary>
	/// registers a system
	/// </summary>
	/// <returns></returns>
	int registerSystem();
	/// <summary>
	/// Gets pointer to box given index
	/// </summary>
	/// <param name="index"></param>
	/// <returns></returns>
	MessageBox* GetBox(int index);
private:
	Messenger();
	Messenger(Messenger const&) = delete;
	void operator=(Messenger const&) = delete;
	//register systems
	//have inbox for each one
	//also have instant
	//down to developer
	//to use properly
	//instant should only be used rarely
	~Messenger();
	std::vector<MessageBox, PoolMessageAllocator<MessageBox>> messageBoxes;
	int size;

};

