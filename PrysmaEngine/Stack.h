#include "SmartPointer.h"
#include "MemoryWrapper.h"
#include <stdlib.h>
#include <vector>
#pragma once
/// <summary>
/// class representing a astack
/// </summary>
class Stack
{
public:

	static Stack& getInstance()
	{
		static Stack m;
		return m;
	}
	
	
	template<typename T, typename... Args>
	/// <summary>
	/// Allocates an  object using args for the constructor
	/// </summary>
	/// <param name="objectRequired">blank object required</param>
	/// <param name="...args">args for the constructor</param>
	/// <returns>smart pointer of correct type</returns>
	SmartPointer<T> allocate(T objectRequired, Args... args);
	/// <summary>
	/// allocates to the stack
	/// </summary>
	/// <param name="size">size of ememry required</param>
	/// <returns>pointer to location</returns>
	void* allocate(size_t size);
	/// <summary>
	/// deallocates using id
	/// </summary>
	/// <param name="id"></param>
	void deallocate(int id);
	/// <summary>
	/// deallocates using pointer
	/// </summary>
	/// <param name="ptr">location to allocate</param>
	void deallocate(void* ptr);
	/// <summary>
	/// gets amount of memeory remaining
	/// </summary>
	/// <returns>amount of memeoryu remaining</returns>
	int memoryRemaining();
	void incrementCounter(int index);
	int decrementCounter(int index);
	~Stack();
	/// <summary>
	/// gets total memeoru
	/// </summary>
	/// <returns>total memeory</returns>
	int GetTotalMemory();
	/// <summary>
	/// sets up the stack
	/// </summary>
	/// <param name="start">start of stack</param>
	/// <param name="end">end of stack</param>
	/// <param name="size">total size of stack</param>
	void SetUp(void* start, void* end, int size);
	/// <summary>
	/// checks if stack is setup
	/// </summary>
	/// <returns></returns>
	bool IsSetUp();
private:
	Stack();
	Stack(void* start, void* end, int size);
	Stack(Stack const&) = delete;
	void operator=(Stack const&) = delete;
	static void* start;
	static void* current;
	static void* end;
	static int totalMemory;
	static bool setup;
	static std::vector<MemoryWrapper> wrappers;

};

template<typename T, typename ...Args>
inline SmartPointer<T> Stack::allocate(T objectRequired, Args ...args)
{
	if (start + sizeof(T) >= end) 
	{
		//throw error
	}
	else 
	{
		//just create object as ususal
		T* obj = new(current) T(args...);

		//add a wrapper as needed this will be long term so it shouldnt be an issue
		wrappers.push_back(MemoryWrapper(wrappers.size()));
		SmartPointer<T> pointer(wrappers.back(), 1);
		current += sizeof(T);
		return pointer;
	}
	
}
