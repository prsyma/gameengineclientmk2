#version 430
layout(location = 0) in vec3 position;

layout(location = 1)uniform mat4 MVP;

out vec4 vColor;
void main()
{
 gl_Position =MVP* vec4(position, 1.0);

  vColor = vec4(0.0,position.y,0.0, 1.0);

 
}