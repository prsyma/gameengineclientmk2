#include "MessageTypes.h"
#include <vector>
#include "PoolMessageAllocator.h"


#pragma once
/// <summary>
/// Message list FIFO
/// </summary>
class MessageList
{
public:
	/// <summary>
	/// Constructor
	/// </summary>
	MessageList();
	/// <summary>
	/// function to store a message
	/// </summary>
	/// <param name="message">message to store</param>
	void StoreMessage(Message message);
	/// <summary>
	/// gets the number of meesages
	/// </summary>
	/// <returns>number of messages</returns>
	int Count();
	/// <summary>
	/// pops a message
	/// </summary>
	/// <returns>takes the first emssage off of the list</returns>
	Message Pop();
	/// <summary>
	/// checks if teh list is empty
	/// </summary>
	/// <returns></returns>
	bool Empty();
private:
	int count = 0;
	std::vector<Message> holders;
	int maxMessages;

};

