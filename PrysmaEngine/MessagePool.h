
#include "MemoryWrapper.h"
#include "SmartPointer.h"
#include <vector>
#pragma once
//defines a block
#ifndef BLOCK
#define BLOCK
struct Block
{


	bool used = false;





};
#endif // !BLOCK


class MessagePool
{
public:
	//meyers singlton thread safe in C++ 11
	static MessagePool& getInstance()
	{
		static MessagePool m;
		return m;
	};
	//gets the blocks
	//has to be done like this otherwise they get lost
	static std::vector<Block>& GetBlocks()
	{
		static std::vector<Block> blocks;
		return blocks;
	};
	//gets wrappers
	static std::vector<MemoryWrapper>& GetWrappers()
	{
		static std::vector<MemoryWrapper> wrappers;
		return wrappers;
	};
	//has it been setip
	bool IsSetUp()
	{
		return setup;
	}
	template<typename T, typename... Args>
	/// <summary>
	/// Allocates an  object using args for the constructor
	/// </summary>
	/// <param name="objectRequired">blank object required</param>
	/// <param name="...args">args for the constructor</param>
	/// <returns>smart pointer of correct type</returns>
	SmartPointer<T> allocate(T objectRequired, Args... args);
	void DecrementCounter(int index);
	void IncrementCounter(int index);
	//gets number of free blocks
	int GetNumberOfFreeBlocks();
	//alocates memeory reutrns a void pointer to the location
	void* allocate(size_t size);
	//frees memory
	void free(void* p);
	//gets the amount of free memeory
	int GetFreeMem();
	//gets total memory
	int totalMem();
	//setups the pool
	void SetUp(void* startOfPool, int sizeOfPool, int sizeOfBlocks);
	~MessagePool() {};
private:
	static int blockSize;
	static int numberOfBlocks;
	static int freeBlocks;
	static std::vector<MemoryWrapper>* wrappers;
	static bool setup;
	static std::vector<Block>* blocks;
	static void* startOfPool;
	//hidden constructor etc
	MessagePool();
	MessagePool(void* startOfPool, int sizeOfPool, int sizeOfBlocks);
	MessagePool(MessagePool const&) = delete;
	void operator=(MessagePool const&) = delete;

	void defragment();
	void deallocate(int wrapperIndex);
	
};
//alocates and reutrn s a smart pointer
template<typename T, typename ...Args>
inline SmartPointer<T> MessagePool::allocate(T objectRequired, Args ...args)
{

	//find the required number of blockes etc and mark them as used... 
	int reqBlocks = sizeof(T) / blockSize + 1;
	int startBlockid = -1;
	int currCount = 0;
	for (int i = 0; i < numberOfBlocks; i++)
	{
		if (currCount == reqBlocks)
		{
			break;
		}
		if ((*blocks)[i].used == true && currCount != reqBlocks)
		{
			startBlockid = -1;
			currCount = 0;
		}
		else
		{
			if (startBlockid == -1)
			{
				startBlockid = i;
			}
			currCount++;
		}

	}
	if (startBlockid == -1)
	{
		//throw error
	}
	else
	{
		//set to done
		freeBlocks -= reqBlocks;
		for (int i = 0; i < reqBlocks; i++)
		{
			(*blocks)[startBlockid + i].used = true;
		}
	}


	//find free wrapper

	for (int i = 0; i < numberOfBlocks; i++)
	{
		if (!(*wrappers)[i].Used())
		{
			(*wrappers)[i].location = startOfPool + (blockSize * startBlockid);
			(*wrappers)[i].size = sizeof(T);
			T* obj = new((*wrappers)[i].location) T(args...);
			SmartPointer<T> pointer((*wrappers)[i], 0);
			return pointer;


		}
	}




}
