#include <vector>
#include "glm.hpp"
#include "PoolAllocator.h"
#include "HeightMap.h"
#pragma once
class TerrainGenerator
{
public:
	TerrainGenerator();
	TerrainGenerator(int nodeCountHeight, int nodeCountWidth, glm::vec3 center, HeightMap* map);
	void index(int height, int width);

	std::vector<glm::vec3, PoolAllocator<glm::vec3>>* GetIndexedNodes();
	std::vector<unsigned int, PoolAllocator<unsigned int>>* GetIndex();
private:
	std::vector<glm::vec3, PoolAllocator<glm::vec3>> terrainNodes;
	std::vector<unsigned int, PoolAllocator<unsigned int>> triangulatedIndicies;
	std::vector<glm::vec3, PoolAllocator<glm::vec3>> indexedNodes;
};

