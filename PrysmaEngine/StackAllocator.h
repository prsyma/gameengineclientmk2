
#include "MemoryManager.h"

#pragma once
template<typename T>
/// <summary>
/// class represenitng a stack alloator
/// </summary>
class StackAllocator
{
private:
	size_t OBJECT_SIZE;
public:
	inline explicit StackAllocator() {};
	/// <summary>
	/// constructor
	/// </summary>
	/// <param name="memSize"></param>
	/// <param name="mem"></param>
	/// <param name="objectSize"></param>
	StackAllocator(size_t memSize, const void* mem, size_t objectSize);
	typedef T value_type;
	typedef value_type* pointer;
	typedef const value_type* const_pointer;
	typedef value_type& reference;
	typedef const value_type& const_reference;
	typedef std::size_t size_type;
	typedef std::ptrdiff_t difference_type;
	virtual ~StackAllocator();
	/// <summary>
	/// function to allocate
	/// </summary>
	/// <param name="size">size of required memeory</param>
	/// <returns>pointrer</returns>
	virtual pointer allocate(size_t size) 
	{
		MemoryManager* m = &MemoryManager::getInstance();

		return (pointer)m->allocateStack(size * sizeof(T));
	};
	virtual void deallocate(void* p, size_type num) ;
	template<typename U>
	struct rebind {
		typedef StackAllocator<U> other;
	};
	template<typename U>
	StackAllocator(const StackAllocator<U>&) throw() {};

	template< class U, class... Args >
	void construct(U* p, Args&& ... args);
};

template<typename T>
inline StackAllocator<T>::StackAllocator(size_t memSize, const void* mem, size_t objectSize)
{
}

template<typename T>
inline StackAllocator<T>::~StackAllocator()
{
}



template<typename T>
/// <summary>
/// function to deallocate
/// </summary>
/// <param name="p">pointer</param>
/// <param name="num">number</param>
inline void StackAllocator<T>::deallocate(void* p, size_type num)
{
	MemoryManager* m = &MemoryManager::getInstance();

	 m->freeStack(p);
}

template<typename T>
template<class U, class ...Args>
/// <summary>
/// function to construct object
/// </summary>
/// <param name="p">pointer/object to construcxt</param>
/// <param name="...args">varaidic agrguments</param>
inline void StackAllocator<T>::construct(U* p, Args&& ...args)
{
	new((void*)p) U(std::forward<Args>(args)...);
}
