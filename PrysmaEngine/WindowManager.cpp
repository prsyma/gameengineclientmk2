#include "WindowManager.h"
MemoryManager* WindowManager::mem = &MemoryManager::getInstance();
Messenger* WindowManager::m = &Messenger::getInstance();
Input* WindowManager::input = &Input::getInstance();

WindowManager::WindowManager()
{
	
	ReadConfig();
	box = m->GetBox(SYSTEMS::GRAPHIC);
	
	
}

void WindowManager::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	//may need to add modifer processing
	input->Set(key,action);
}

void WindowManager::cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
	input->SetMouseLocation(xpos, ypos);
}

WindowManager::~WindowManager()
{
}

bool WindowManager::ShouldClose()
{
	return glfwWindowShouldClose(window);
}

void WindowManager::Draw()
{
	//get the list to render
		//we wait for the list to be compiled
	//glEnable(GL_CULL_FACE);
	//glCullFace(GL_BACK);
	Message mes = box->pop();
	glClearColor(0.2f, 0.3f, 0.5f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	if (mes.type == MessageType::DRAWLIST)
	{
		DrawList* l = (DrawList*)mes.data;
		//we then can bind etc...
		for (int i = 0; i < l->items.size(); i++)
		{
			l->items[i].Render();

		}
	}

	glfwSwapBuffers(window);
	glfwPollEvents();
}

void WindowManager::Stop()
{
	glfwTerminate();
}

void WindowManager::ReadConfig()
{
	//config
	//ACTION: KEY
	
}



void WindowManager::Start()
{
	if (!glfwInit())
	{

	}
	window = glfwCreateWindow(w, h, "Prysma Engine", NULL, NULL);
	glfwSetKeyCallback(window, key_callback);
	glfwSetCursorPosCallback(window, cursor_position_callback);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	Message msg;
	msg.from = SYSTEMS::WINDOW;
	msg.to = SYSTEMS::SCENE;
	msg.type = MessageType::DRAW;
	
	glfwMakeContextCurrent(window);
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);	
}
