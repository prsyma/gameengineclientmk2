#ifndef GRAVITYSYSTEM
#include "ECS.h"
#include "Components.h"
#pragma once
class GravitySystem : public ECS::System
{
public:
	GravitySystem(float Amount);
	virtual ~GravitySystem() {};
	virtual void Update(ECS::World* world) override;
private:
	float gravAmount;
};

#endif // !GRAVITYSYSTEM

