#ifndef INPUT_SYSTEM
#define INPUT_SYSTEM
#include "ECS.h"
#include "Input.h"
#include "enums.h"
#include "Components.h"
#pragma once
class InputSystem : public ECS::System 
{
public:
	void Register(ECS::World* world) override;

	void Update(ECS::World* world) override;

	void Unregister(ECS::World* world) override;

	~InputSystem() override;
private:
	static Input* input;
	float speed = 50000.00f;
	ECS::Entity* player;
	bool first = true;
	MouseLocation previous;
};

#endif