#include "Network.h"
#include <string>
#include <iostream>
MemoryManager* Network::mem = &MemoryManager::getInstance();
Messenger* Network::m = &Messenger::getInstance();
bool Network::running = false;
bool Network::authenicated = false;
ECS::World* Network::world = nullptr;
std::string Network::connectionAddress = "http://127.0.0.1/";
MessageBox* Network::box = nullptr;
cpr::Cookies Network::cookies = cpr::Cookies();
int Network::id = 0;
Network::Network()
{

}
Network::~Network()
{
}
//this is send updates
int Network::checkMessages()
{
	PoolMessageAllocator<Message> p;
	PoolMessageAllocator<nlohmann::json> jsonAlloc;
	PoolMessageAllocator<PositionUpdate> posAlloc;
	while (running) 
	{

		if (!authenicated)
		{
			//authenticate
		}
		else
		{
			//pass messages on
			//currently they will be update messages so just a thing
			//we dont need to send id as it should be in session

			//std::this_thread::sleep_for(sleepTime);
			while (box->NotEmpty())
			{
				Message* m = new(p.allocate(1)) Message;
				*m = box->pop();
				nlohmann::json j;
				if (m->type == MessageType::POSITION)
				{
					//we need to encode 
					PositionUpdate* p = (PositionUpdate*)m->data;

					std::string t = "";
					std::string posx = std::to_string(p->position.x);
					std::string posy = std::to_string(p->position.y);
					std::string posz = std::to_string(p->position.z);
					std::string health = std::to_string(p->health);
					std::string area = std::string(p->area);
					area = area.substr(0, 2);
					std::string idS = std::to_string(id);
					//t = p->area[0]+p->area[1];
					auto r = cpr::Post(cpr::Url{ connectionAddress + "send" },
						cpr::Payload{
						{"x", posx},
						{"y", posy},
						{"z",  posz},
						{"health", health},
						{"area",area},
						{"id",idS}
						}, cookies);
					posAlloc.deallocate(p, 1);
				}
			}




		}
	}
		
	
	return 0;
}
//this gets updates
int Network::GetUpdates()
{
	PoolMessageAllocator<Message> p;
	PoolMessageAllocator<PositionUpdate> posAlloc;
	//make request to server to recieve data
	//will be run on separate thread entierly
	//may add shut down feature to the message box
	
		//std::this_thread::sleep_for(sleepTime);
	while (running) 
	{
		if (!authenicated)
		{
			//authenticate
		}
		else
		{
			//get messages
			auto r = cpr::Get(cpr::Url{ connectionAddress + "GetUpdates" }, cookies);
			//we get id etc
			if (r.status_code == 200)
			{
				try
				{
					auto json = nlohmann::json::parse(r.text);
					//bit of a mess
					//there will be a known number of areas 
					//so complexity is
					//n * m
					//could limit down the line the only the closest ones so
					//max would be 9?
					//cureent zone plus surrounding 8
					//send the decoded updates to the scene manager
					for (auto& x : json.items())
					{
						//area id
						std::string id = x.key();
						//we know that the value will be structured
						std::string val = x.value();
						auto json2 = nlohmann::json::parse(val);
						//each entity
						for (auto& y : json2.items())
						{
							//check which user it is it is in format 
							//areaUserid
							//we need to split
							//for now a user only

							std::string entId = y.key();
							entId = entId.substr(6);
							std::string data = y.value();
							auto json3 = nlohmann::json::parse(data);
							PositionUpdate* posUp = posAlloc.allocate(1);
							posAlloc.construct(posUp);

							for (auto& z : json3.items())
							{
								std::string key = z.key();
								//todo fix this
								if (key == "area")
								{
									std::string data = z.value();
									posUp->area = data;
								}
								if (key == "ID" || key == "id")
								{
									posUp->id = std::stoi(entId);
								}

								if (key == "health")
								{
									std::string data = z.value();
									posUp->health = std::stoi(data);
								}
								if (key == "x")
								{
									std::string data = z.value();
									posUp->position.x = std::stof(data);
								}
								if (key == "y")
								{
									std::string data = z.value();
									posUp->position.y = std::stof(data);
								}
								if (key == "z")
								{
									std::string data = z.value();
									posUp->position.z = std::stof(data);
								}




							}

							//just update directly instead of having to send message
							//this avoids synchronisation issues
							//as teh data at most will be one or two updates out of date
							if (posUp->id != world->GetPlayer()->getEntityId())
							{
								ECS::Entity* ent = world->getById(posUp->id);
								if (ent == nullptr)
								{
									ent = world->create(posUp->id);
									auto entTrans = ent->assign<Transform>();
									entTrans->postion = posUp->position;
									auto b = ent->assign<Health>();
									b->health = posUp->health;
									auto r = ent->assign<Render>();
									r->gi = *world->playerModel;
								}
								else
								{
									auto e = ent->get<Transform>();
									e->postion = posUp->position;
									auto b = ent->get<Health>();
									b->health = posUp->health;
								}

							}

						}

					}
				}
				catch (nlohmann::json::type_error t)
				{
					std::cout << t.what() << std::endl;
				}






			}
		}
	}
		
	auto r = cpr::Get(cpr::Url{ connectionAddress + "logout" }, cookies);
	return 0;
}
void Network::run(ECS::World* w)
{
	world = w;
	running = true;
	box = m->GetBox(SYSTEMS::NETWORK);
	std::thread(checkMessages).detach();
	std::thread(GetUpdates).detach();
	//lauch two threads

}

bool Network::authenticate(std::string user, std::string pass, std::string connection)
{
	auto r = cpr::Post(cpr::Url{ connection +"/login" },
	cpr::Payload{ {"username", user},{"password",pass } });
	//we get id etc
	if (r.status_code == 200) 
	{
		cookies = r.cookies;
		try
		{
			auto json = nlohmann::json::parse(r.text.c_str());
			id = json.at("ID");
			if (id > 0)
			{
				authenicated = true;
				connectionAddress = connection + "/";

				return true;
			}
		}
		catch (nlohmann::json::parse_error& e) 
		{
			std::cout << "message: " << e.what() << '\n'
				<< "exception id: " << e.id << '\n'
				<< "byte position of error: " << e.byte << std::endl;
		}
		
		return false;
		
	}
	return false;
}

int Network::getId()
{
	return id;
}

void Network::shutdown()
{
	running = false;
}

