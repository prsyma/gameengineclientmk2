#pragma once
#include <vector>
#include "PoolAllocator.h"
#include "glm.hpp"
#include "Components.h"
#include "Glad/glad.h"
#include "GLM/gtc/type_ptr.hpp"
/// <summary>
/// draw list item struct
/// </summary>
struct DrawListItem 
{
	DrawListItem() {};
	DrawListItem(GraphicItem g) 
	{
		gi = g;
	}
	GraphicItem gi;
	/// <summary>
	/// function to render
	/// </summary>
	void Render();
};
/// <summary>
/// draw list struct
/// </summary>
struct DrawList 
{

	int numOfItems;
	std::vector<DrawListItem, PoolAllocator<DrawListItem>> items;
	
};