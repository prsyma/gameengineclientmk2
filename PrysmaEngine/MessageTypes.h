#include <string>
#include <vector>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/find_iterator.hpp>
#include <glm.hpp>
#pragma once
/// <summary>
/// enum of message types
/// </summary>
enum MessageType
{
	POSITION,
	KEYUPDATE,
	DRAW,
	DRAWLIST
};
/// <summary>
/// struct containg the key info types
/// </summary>
struct KeyInfo
{
	int key;
	int mods;
	int action;

};
/// <summary>
/// message struct
/// </summary>
struct Message
{
public:
	/// <summary>
	/// default constructor
	/// </summary>
	Message() {};
	/// <summary>
	/// cpoy constructor
	/// </summary>
	Message(const Message& m) : to(m.to), from(m.from), type(m.type),data(m.data){};
	Message& operator=(Message m) 
	{
		to = m.to;
		from = m.from;
		type = m.type;
		data = m.data;
		return *this;
	};
	int to;
	int from;
	/// <summary>
	/// type of message
	/// </summary>
	MessageType type;
	/// <summary>
	/// pointer to data
	/// </summary>
	void* data;
	~Message() {};
};
struct KeyMessage 
{
	KeyInfo k;
};
/// <summary>
/// poisiton  update streuct 
/// </summary>
struct PositionUpdate 
{
	PositionUpdate() 
	{
		id = -1;
		position = glm::vec3(0.0f);
		health = 0;
		area = "";
	};
	//need to add in qvector 3
	unsigned int id;
	glm::vec3 position;
	//glm::vec3 rotation;
	int health;
	std::string area;
};
