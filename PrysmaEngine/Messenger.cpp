#include "Messenger.h"

void Messenger::Send(int dest, Message m)
{
	messageBoxes[dest].RecieveNormal(m);
}

void Messenger::Send_Instant(int dest, Message m)
{
	messageBoxes[dest].RecieveInstant(m);
}

void Messenger::Broadcast(Message m)
{
	for (int i =0; i < messageBoxes.size();i++) 
	{
		messageBoxes[i].RecieveNormal(m);
	}
}

int Messenger::registerSystem()
{
	messageBoxes.push_back(MessageBox(messageBoxes.size()));
	return messageBoxes.size()-1;
}

MessageBox* Messenger::GetBox(int index)
{
	return &messageBoxes[index];
}

Messenger::Messenger()
{
	for (int i = SYSTEMS::SCENE; i < SYSTEMS::WINDOW; i++) 
	{
		messageBoxes.push_back(MessageBox(messageBoxes.size()));
	}
	
}

Messenger::~Messenger()
{
}
