#include "TerrainGenerator.h"

TerrainGenerator::TerrainGenerator()
{
}

TerrainGenerator::TerrainGenerator(int nodeCountHeight, int nodeCountWidth, glm::vec3 center, HeightMap* map)
{
	float currentWidht = -1.0f;
	float currentHeight = 1.0f;
	float heightDecrease = 2.0f / nodeCountHeight;
	float widthIncrease = 2.0f / nodeCountWidth;
	float currentHeightY = 0.0f;
	for (int i = 0; i < nodeCountHeight; i++)
	{
		for (int j = 0; j < nodeCountWidth; j++)
		{
			
				float xVal = (float)(nodeCountHeight - i) / nodeCountHeight;
				float yVal = (float)j / nodeCountWidth;
				currentHeightY = map->nearestNeighbourValue(xVal, yVal);
			



			terrainNodes.push_back(glm::vec3(currentHeight, currentHeightY/255.0f, currentWidht));
			currentWidht += widthIncrease;

		}
		currentHeight -= heightDecrease;
		currentWidht = -1.0f;
	}

	index(nodeCountHeight, nodeCountWidth);
}

void TerrainGenerator::index(int height, int width)
{
	for (int heightCount = 0; heightCount < height - 1; heightCount++)
	{
		for (int widthCount = 0; widthCount < width - 1; widthCount++)
		{
			//row i, index j
			triangulatedIndicies.push_back((heightCount * width) + widthCount);
			//row i+1, index j
			triangulatedIndicies.push_back(((heightCount + 1) * width) + widthCount);
			//row i+1, index j+1
			triangulatedIndicies.push_back(((heightCount + 1) * width) + widthCount + 1);
			////row i, index j
			triangulatedIndicies.push_back((heightCount * width) + widthCount);
			//row i+1, index j+1
			triangulatedIndicies.push_back(((heightCount + 1) * width) + widthCount + 1);
			//rowi, j+1
			triangulatedIndicies.push_back(((heightCount)* width) + widthCount + 1);
		}

	}


	for (int i = 0; i < triangulatedIndicies.size(); i++)
	{
		indexedNodes.push_back(terrainNodes[triangulatedIndicies[i]]);
	}
}


std::vector<glm::vec3, PoolAllocator<glm::vec3>>* TerrainGenerator::GetIndexedNodes()
{
	return &indexedNodes;
}

std::vector<unsigned int, PoolAllocator<unsigned int>>* TerrainGenerator::GetIndex()
{
	return &triangulatedIndicies;;
}
