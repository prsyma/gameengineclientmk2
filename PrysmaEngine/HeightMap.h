#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include "glm.hpp"
#ifndef HEIGHTMAP
#define HEIGHTMAP

/// <summary>
/// Class for a height map/grey map
/// </summary>
class HeightMap
{
public:
	/// <summary>
	/// constructor with know width and height of the map
	/// </summary>
	/// <param name="w">width in pixels</param>
	/// <param name="h">height in pixels</param>
	HeightMap(int w, int h, glm::vec3 scale);
	/// <summary>
	/// default constructor
	/// </summary>
	HeightMap();
	/// <summary>
	/// destructor
	/// </summary>
	~HeightMap();
	/// <summary>
	/// width of map
	/// </summary>
	int width;
	/// <summary>
	/// height of map
	/// </summary>
	int height;
	/// <summary>
	/// int ** for the map of values
	/// </summary>
	int** values;
	/// <summary>
	/// used to get the nearest value to the given locations
	/// </summary>
	/// <param name="Height">height in world location</param>
	/// <param name="width">width in world location</param>
	/// <returns>nearest height value</returns>
	int nearestNeighbourValue(float Height, float width);
	/// <summary>
	/// Function to read the map
	/// </summary>
	/// <param name="filePath">file path to read</param>
	void ReadMap(std::string filePath);

	char name[2];
	glm::vec3 scale;
};

#endif 
