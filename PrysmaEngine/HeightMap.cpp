#include "HeightMap.h"
/// <summary>
/// default constructor
/// </summary>
HeightMap::HeightMap()
{
	//ctor
}
/// <summary>
/// destructor
/// </summary>
HeightMap::~HeightMap()
{
}
/// <summary>
/// constructor with know width and height of the map
/// </summary>
/// <param name="w">width in pixels</param>
/// <param name="h">height in pixels</param>
HeightMap::HeightMap(int w, int h,  glm::vec3 scale)
{
	this->scale = scale;
	//we create the array
	values = new int* [h];
	for (int i = 0; i < h; i++)
	{
		values[i] = new int[w];
	}

	for (int i = 0; i < h; i++)
	{
		for (int j = 0; j < w; j++)
		{
			values[i][j] = 0;
		}
	}
}

/// <summary>
/// Function to read the map
/// </summary>
/// <param name="filePath">file path to read</param>
void HeightMap::ReadMap(std::string filePath)
{
	//if the file path is NONE then we do not have a file given
	if (std::strcmp(filePath.c_str(), "NONE") == 0)
	{
		return;
	}
	//create if stream
	std::ifstream inFile;
	//open stream
	inFile.open(filePath.c_str());
	//vector for the map heights to be read into  serialy, we will reconstruct array later
	std::vector<int> mapHeights;
	//width is current 0
	width = 0;
	//height is currently 0
	height = 0;
	//if we have opened the file
	if (inFile.is_open())
	{
		//we start getting lines
		std::string line = "";
		//the first line is the magic number
		bool firstLine = true;
		int lineCount = 0;
		while (std::getline(inFile, line))
		{
			//the magic number sshould be P2
			if (line[0] == 'P' && line[1] == '2')
			{
				//we have title
				firstLine = false;
				lineCount++;
				continue;
			}
			if (firstLine)
			{
				//this means we havent found the magic number
				std::cout << "Wrong File Type required PGM" << std::endl;
				break;
			}
			//a comment isnt neccessarily needed
			if (line[0] == '#')
			{
				//comment
				lineCount++;
				continue;
			}
			//the third line should be the map widht and height so read
			if (lineCount == 2)
			{
				lineCount++;
				std::stringstream s(line);
				s >> width;
				s >> height;
				//reserve the required memeory
				mapHeights.reserve(height * width);
			}
			//line 4 should be the max colour values(255)
			if (lineCount == 3)
			{
				//max value

				lineCount++;
				int val;
				std::stringstream s(line);
				s >> val;
				continue;
			}

			//we can then push back into vector
			//create string stream since we do not know howmany colours are on each line
			std::stringstream stream(line);
			while (stream.good())
			{
				int value = 0;
				stream >> value;
				mapHeights.push_back(value);
			}
		}
	}
	else
	{
		std::cout << "Error File Cannot Be Read!!!!" << std::endl;
		return;
	}

	//we now should have the required values
	//intilise the 2d array
	values = new int* [height];
	for (int i = 0; i < height; i++)
	{
		values[i] = new int[width];
	}
	//loop through and set up required values
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			values[i][j] = mapHeights[(i * width) + j];
		}
	}
}
/// <summary>
/// used to get the nearest value to the given locations
/// </summary>
/// <param name="Height">height in UV</param>
/// <param name="width">width in UV</param>
/// <returns>nearest height value</returns>
int HeightMap::nearestNeighbourValue(float height, float width)
{
	//areas are 1000 x 1000
	//get UV for area
	height = height;
	width = width;
	//converts the given height into the nearest h value for this map
	int h = (int)(height * this->height);
	if (h >= this->height)
	{
		//if given height is 1.0, then that would be out of range of the array
		//so
		h = this->height - 1;
	}
	//converts given width to map width
	int w = (int)(width * this->width);
	if (w >= this->width)
	{
		//if given width is 1.0 then it  would be out of range of array
		w = this->width - 1;
	}
	//return the nearest value multiplied by scale value
	return values[h][w];
}