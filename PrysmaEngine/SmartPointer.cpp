#include "MemoryManager.h"
#include "SmartPointer.h"


template<typename T>
SmartPointer<T>::SmartPointer(MemoryWrapper* wrapper, int location)
{
	size = sizeof(T);
	this->id = wrapper->id;
	this->dataLocation = location;
	this->wrapper = wrapper;
}
template<typename T>
inline SmartPointer<T>::~SmartPointer()
{
	MemoryManager& m = MemoryManager::getInstance();
	m.DecrementCounter(dataLocation, id);

}

template<typename T>
SmartPointer<T>::SmartPointer(const SmartPointer<T>& pointerToCopy)
{
}

template<typename T>
SmartPointer<T>& SmartPointer<T>::operator=(const SmartPointer<T>& pointerToAssign)
{
	if (this != &pointerToAssign) 
	{
		dataLocation = pointerToAssign.dataLocation;
		wrapper = pointerToAssign.wrapper;
		id = pointerToAssign.id;
		size = pointerToAssign.size;
		MemoryManager& m = MemoryManager::getInstance();
		m.IncrementCounter(dataLocation, id);
		
	}
	return *this;
}

template<typename T>
bool SmartPointer<T>::operator==(SmartPointer<T>& pointer)
{
	if (this->wrapper->getLocation() == pointer.wrapper->getLocation()) 
	{
		return true;
	}
	return false;
}

template<typename T>
T& SmartPointer<T>::operator*()
{
	return *(T*)wrapper->getLocation();
}

template<typename T>
T* SmartPointer<T>::operator->()
{
	return (T*)wrapper->getLocation();
}
