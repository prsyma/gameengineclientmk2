#include "Stack.h"

bool Stack::setup = false;
void* Stack::start = nullptr;
void* Stack::end = nullptr;
void* Stack::current = nullptr;
int Stack::totalMemory = 0;
std::vector<MemoryWrapper> Stack::wrappers = std::vector<MemoryWrapper>();
Stack::Stack()
{
}

Stack::Stack(void* start, void* end, int size)
{
	this->start = start;
	this->current = this->start;
	this->end = end;
	this->totalMemory = size;
	
}

void* Stack::allocate(size_t size)
{

	if ((char*)start + size >= end)
	{
		//throw error
	}
	else
	{
		
		wrappers.push_back(MemoryWrapper(wrappers.size()));
		
		MemoryWrapper* w= &wrappers.back();
		w->size = size;
		w->location = current;
		void* ptr = current;
		current  = (char*)current+size;
		return ptr;
	}
	return nullptr;
}

void Stack::deallocate(int id)
{
	//we have the id of the wrapper
	//we need to see if minusing the value
	//if this deallocation is the most recent addition
	if (wrappers.back().id == id) 
	{
		//we can just remove it 
		//we also have to check for ones before it 
		//wrappers.back().markedForDealloc = true;
		int currentId = wrappers.size() - 1;
		//skip the end one
		for (int i = wrappers.size() - 2;i<0;i--)
		{
			//we have to check going back wards if there are others marked for dealloc
			if (wrappers[i].markedForDealloc == true) 
			{
				currentId--;
			}
			else 
			{
				break;
			}
		}
		current = wrappers[currentId].location;
		//we now need to remove the wrappers
		wrappers.erase(wrappers.begin() + currentId, wrappers.end());
	}
	else 
	{
		//search for wrapper
		for (int i = 0; i++;i < wrappers.size())
		{
			if (wrappers[i].id == id) 
			{
				wrappers[i].markedForDealloc = true;
			}
		}
	}
}

void Stack::deallocate(void* ptr)
{
	//we have the id of the wrapper
	//we need to see if minusing the value
	//if this deallocation is the most recent addition
	if (wrappers.back().location == ptr)
	{
		//we can just remove it 
		//we also have to check for ones before it 
		//wrappers.back().markedForDealloc = true;
		int currentId = wrappers.size() - 1;
		//skip the end one
		for (int i = wrappers.size() - 2; i < 0; i--)
		{
			//we have to check going back wards if there are others marked for dealloc
			if (wrappers[i].markedForDealloc == true)
			{
				currentId--;
			}
			else
			{
				break;
			}
		}
		current = wrappers[currentId].location;
		//we now need to remove the wrappers
		wrappers.erase(wrappers.begin() + currentId, wrappers.end());
	}
	else
	{
		//search for wrapper
		for (int i = 0; i++; i < wrappers.size())
		{
			if (wrappers[i].location == ptr)
			{
				wrappers[i].markedForDealloc = true;
			}
		}
	}
}

int Stack::memoryRemaining()
{
	int val =  ((char*)current - start);
	int val2 = ((char*)end - start);
		return val2 - val;
}

void Stack::incrementCounter(int index)
{
	wrappers[index].counter.increment();
}

int Stack::decrementCounter(int index)
{
	return wrappers[index].counter.decrement();
}

Stack::~Stack()
{
	//free(start);
}

int Stack::GetTotalMemory()
{
	return totalMemory;
}

void Stack::SetUp(void* start, void* end, int size)
{
	setup = true;
	this->start = start;
	this->current = this->start;
	this->end = end;
	this->totalMemory = size;
}

bool Stack::IsSetUp()
{
	return setup;
}
