// PrysmaEngine.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>


#include "StackAllocator.h"
#include "Network.h"
#include "ECS.h"
#include "InputSystem.h"
#include "PlayerUpdateSystem.h"
#include "RenderSystem.h"
#include "UpdateSystem.h"
#include "Components.h"
#include "WindowManager.h" 
#include "HeightMap.h"
#include "TerrainGenerator.h"
#include "GraphicsHashSytem.h"

GraphicsHashSytem* sys;
GraphicItem PlayerModel;
bool authenticate(std::string username, std::string password, std::string connectionServer, Network* net)
{
	//use network to actuall autehnticate
	
	return true;
}
void SetUpWorld(ECS::World* world, int playerID) 
{
	//setup the world
	ECS::Entity* p = world->create(playerID);
	world->SetPlayer(p);
	//set up the player
	auto playerTrans = p->assign<Transform>();
	playerTrans->area = "01";
	playerTrans->yawDegrees = -90.0f;
	//playerTrans->RotateY(90.0f, playerTrans->Left());
	p->assign<Health>();
	auto cam = p->assign<Camera>();
	//setup camera
	cam->transform.postion = glm::vec3(0, 2, 0);
	PlayerModel = sys->Load("player.vert", "player.frag", "cube.obj");
	world->playerModel = &PlayerModel;
	//apply the rotation
	
	//just load player model...

	//we now load the other objects....
	HeightMap map01 = HeightMap();
	map01.ReadMap("map01.pgm");
	map01.scale = glm::vec3(100, 10, 100);
	map01.name[0] = '0';
	map01.name[1] = '1';
	TerrainGenerator gen = TerrainGenerator(1000,1000,glm::vec3(0,0,0),&map01);
	GraphicItem giTerrain = sys->LoadTerrain("terrain.vert", "terrain.frag",*gen.GetIndexedNodes() , world);
	//we now need to add terrain as entity
	ECS::Entity* terrain01 = world->create(500);
	terrain01->assign<Render>(giTerrain);
	ECS::ComponentWrapper<Transform> t= terrain01->assign<Transform>();
	t->scale = map01.scale;
	t->postion = glm::vec3(-100, 0, -100);
	t->pitchDegrees = 0.0f;
	t->yawDegrees = 0.0f;
	t->GetRotation();
	
	world->RegisterHeightMap(map01);
	//systems
	StackAllocator<PlayerUpdateSystem> pAlloc;
	StackAllocator<InputSystem> iAlloc;
	StackAllocator<RenderSystem> rAlloc;
	
	PlayerUpdateSystem* pus = pAlloc.allocate(1);
	pAlloc.construct(pus);
	InputSystem* inSys = iAlloc.allocate(1);
	iAlloc.construct(inSys);
	RenderSystem* render = rAlloc.allocate(1);
	rAlloc.construct(render);
	
	world->registerSystem(inSys);
	world->registerSystem(pus);
	world->registerSystem(render);

	//we then need to activate system
	world->enableSystem(inSys);
	world->enableSystem(pus);
	world->enableSystem(render);
	//we shoudl now be good to go

}

int main()
{
	//set up the memeory manager
	//then the message system
	//then register systems
	//then we can load the first system
    std::cout << "Hello World!\n";
	//we should have login loop here
	std::string username = "";
	std::string password = "";
	std::string connectionServer = "";
	//we need to login
	//so we setup the network
	StackAllocator<Network> alloc;
	int t = sizeof(Network);
	Network* network = alloc.allocate(1);
	alloc.construct(network);


	StackAllocator<WindowManager> allocWin;
	//*network = Network();
	WindowManager* winMan = allocWin.allocate(1);
	allocWin.construct(winMan);
	//*winMan = WindowManager();
	bool authed = false;
	
	
	while (!authed)
	{
		std::cout << "Please enter your Connection server IP: ";
		std::cin >> connectionServer;
		std::cout << "\nPlease enter your Username: ";
		std::cin >> username;
		std::cout << "\nPlease enter your Password: ";
		std::cin >> password;
		authed = network->authenticate(username, password, connectionServer);
	}
	if (authed) 
	{
		PoolAllocator<GraphicsHashSytem> hashAlloc;
		ECS::World world;
		world.CreateWorld();
		sys = hashAlloc.allocate(1);
		hashAlloc.construct(sys);
		

		//we can start the window
		winMan->Start();
		SetUpWorld(&world,network->getId());
		network->run(&world);
		//tick ECS
		while (!winMan->ShouldClose()) 
		{
			//update ECS
			world.tick();
			//draw
			winMan->Draw();
			
		}
		network->shutdown();
		winMan->Stop();
		
	}
	

}


