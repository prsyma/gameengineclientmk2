#include <glm.hpp>
#include <gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GLM/gtc/quaternion.hpp>
#include <GLM/gtx/quaternion.hpp>
#include "Dependancies/GLFW/include/GLFW/glfw3.h"
#include "Glad/glad.h"
#pragma once
//Struct Represeting a transform
//contains position rotation and scale
//as well as helper functions
struct Transform 
{
	Transform(float x, float y, float z, float rot) 
	{
		postion.x = x;
		postion.y = y;
		postion.z = z;
		
	
		scale = glm::vec3(1.0f);
	
		
	};
	Transform() 
	{
		postion.x = 0.0f;
		postion.y = 1.0f;
		postion.z = 0.0f;
		
		scale = glm::vec3(1.0f);
	
	}
	//gets rotation as a matrix we only actually rotate around two axis
	glm::mat4 GetRotation(Transform* parent = nullptr) 
	{
		
		glm::mat4 rotx = glm::rotate(glm::radians(pitchDegrees), GetCamUp());
		glm::mat4 roty = glm::rotate(glm::radians(yawDegrees), GetCamRight());
		glm::mat4 m = roty * rotx;
		return m;
	}
	
	//gets teh mopdel viwe projection matrix
	glm::mat4x4 GetMVP(glm::mat4x4 proj, glm::mat4x4 view) 
	{
		glm::mat4 scaleMat = glm::scale(glm::mat4(1.0f), scale);
		glm::mat4 translation = glm::translate(glm::mat4(1.0f), postion);
		
		glm::mat4 rotation = GetRotation();

		glm::mat4 model = translation*rotation*scaleMat;

		glm::mat4 MVP = proj * view * model;
		return MVP;
	}
	
	glm::vec3 postion;
	//rotation around up
	float pitchDegrees = 0.0f;
	float yawDegrees = 0.0f;
	//gets the current front of the camera
	//the angel is lomited so we dont get gimbal lock
	glm::vec3 GetCamFront() 
	{
		if (pitchDegrees > 89.0f)
			pitchDegrees = 89.0f;
		if (pitchDegrees < -89.0f)
			pitchDegrees = -89.0f;

		glm::vec3 front;
		front.x = cos(glm::radians(yawDegrees)) * cos(glm::radians(pitchDegrees));
		front.y = sin(glm::radians(pitchDegrees));
		front.z = sin(glm::radians(yawDegrees)) * cos(glm::radians(pitchDegrees));
		return glm::normalize(front);
	}
	//gets the front of the camera/the direction we are to move in as we move along the ground
	glm::vec3 GetCamMoveFront()
	{
		glm::vec3 front;
		front.x = cos(glm::radians(yawDegrees)) * cos(glm::radians(0.0f));
		front.y = sin(glm::radians(0.0f));
		front.z = sin(glm::radians(yawDegrees)) * cos(glm::radians(0.0f));
		return glm::normalize(front);
	}
	//get move right
	glm::vec3 GetCamMoveRight()
	{
		return glm::normalize(glm::cross(GetCamMoveFront(), up));
	}
	//get actual right
	glm::vec3 GetCamRight() 
	{
		return glm::normalize(glm::cross(GetCamFront(), up));
	}
	//getr camera up
	glm::vec3 GetCamUp() 
	{
		return  glm::normalize(glm::cross(GetCamRight(), GetCamFront()));
	}
	glm::vec3 scale;
	glm::vec3 direction = glm::vec3(0,0,1);

	Transform& operator=(const Transform& t)
	{
		this->postion = t.postion;
		
		
		this->scale = t.scale;
		return *this;
	}
	std::string area;
private:
	const glm::vec3 front = glm::vec3(0.0f, 0.0f, 1.0f);
	const glm::vec3 left = glm::vec3(-1.0f, 0.0f, 0.0f);
	const glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);

	

};
//struct for health
struct Health 
{
	Health() 
	{
		health = 100;
	}
	int health;
};
//struct  for graphic item
struct GraphicItem
{
	unsigned int VAO;
	unsigned int program;
	unsigned int vertLength;
	glm::mat4x4 MVP;
} ;
//struct for render
struct Render 
{
	Render() {};
	Render(GraphicItem gi) 
	{
		this->gi = gi;
	};
	GraphicItem gi;
};
//struct represneting a camrea
struct Camera 
{
	Transform transform;
	glm::mat4x4 view;
	glm::mat4x4 proj;
	void GenereateVP(Transform parent)
	{
		view = glm::lookAt(transform.postion+ parent.postion, parent.GetCamFront()+parent.postion+transform.postion, parent.GetCamUp());
		
		proj = glm::perspective(glm::radians(90.0f), 4.0f / 3.0f, 0.1f, 1000.0f);
		//proj[1, 1] = proj[1, 1] * -1.0f;
	}
};