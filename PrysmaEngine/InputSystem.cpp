#include "InputSystem.h"
Input* InputSystem::input = &Input::getInstance();

void InputSystem::Register(ECS::World* world)
{
	player = world->GetPlayer();
}

void InputSystem::Update(ECS::World* world)
{
	//m/s
	//float movementSpeed = 5;
	float delta = world->GetDelta();
	//if input.forward etc
	ECS::ComponentWrapper<Transform> loc = player->get<Transform>();
	ECS::ComponentWrapper<Camera> cam = player->get<Camera>();
	//we have the player data
	//get front etc
	if (input->Get(KEYS::FORWARD)) 
	{
		loc->postion += loc->GetCamMoveFront() * (delta * speed);
	}
	else if (input->Get(KEYS::BACK))
	{
		loc->postion -= loc->GetCamMoveFront() * (delta * speed);
	}
	else if (input->Get(KEYS::LEFT))
	{
		loc->postion -= loc->GetCamMoveRight()* (delta * speed);
	}
	else if (input->Get(KEYS::RIGHT))
	{
		loc->postion += loc->GetCamMoveRight()* (delta * speed);
	}

	//we also need to get the camera/player

	if (first == true)
	{
		previous = *(input->GetMouseLocation());
		first = false;
	}
	MouseLocation current = *input->GetMouseLocation();
	//std::cout << "Mouse Position old: " << previous.x << " " << previous.y << "\nNew: " << current.x << " " << current.y << std::endl;
	float xDiff = current.x - previous.x;
	float yDiff = current.y - previous.y;
	//top left is 0,0 so if xOld- x is positive we are turning left
	//if x is negative then we are turning right
	//if yold - ynew is negative then we are turning down
	//if yold - ynew is positive then we are turing up

	//up down we rotate the camera 
	//left right we rotate the player
	//forward for character is only dependant on leftr right rotation

	loc->yawDegrees += xDiff * 0.05f;
	loc->pitchDegrees -= yDiff * 0.05f;
	if (loc->pitchDegrees > 89.0f)
		loc->pitchDegrees = 89.0f;
	if (loc->pitchDegrees < -89.0f)
		loc->pitchDegrees = -89.0f;
	char area[2];
	area[0] = '0';
	area[1] = '1';
//	loc->postion.y = world->GetMinHeight(loc->postion.x, loc->postion.x, area)*10;
	previous = current;
	
}

void InputSystem::Unregister(ECS::World* world)
{
}

InputSystem::~InputSystem()
{
}
