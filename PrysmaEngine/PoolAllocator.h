
#include "MemoryManager.h"

#pragma once
template<typename T>
/// <summary>
/// class represening the memeory allcoator for the  Pool
/// </summary>
class PoolAllocator
{
private:
	size_t OBJECT_SIZE;
public:
	//default requireements
	PoolAllocator(size_t memSize, const void* mem, size_t objectSize);
	typedef T value_type;
	typedef value_type* pointer;
	typedef const value_type* const_pointer;
	typedef value_type& reference;
	typedef const value_type& const_reference;
	typedef std::size_t size_type;
	typedef std::ptrdiff_t difference_type;
	virtual ~PoolAllocator();
	pointer address(reference value) const {
		return &value;
	}
	const_pointer address(const_reference value) const {
		return &value;
	}
	/// <summary>
	/// returns allocatd pointer memeory
	/// </summary>
	/// <param name="size">required amount of memeory</param>
	/// <returns>pointer</returns>
	virtual pointer allocate(size_t size)
	{
		MemoryManager* m = &MemoryManager::getInstance();

		return (pointer)m->allocatePool(size * sizeof(T));
	};
	virtual void deallocate(void* p, size_type num);

	template<typename U>
	struct rebind {
		typedef PoolAllocator<U> other;
	};
	PoolAllocator() throw() {};
	template<typename U>
	PoolAllocator(const PoolAllocator<U>&) throw() {};
	template< class U, class... Args >
	void construct(U* p, Args&& ... args);
	
};

template<typename T>
inline PoolAllocator<T>::PoolAllocator(size_t memSize, const void* mem, size_t objectSize) : OBJECT_SIZE(objectSize)
{
}

template<typename T>
inline PoolAllocator<T>::~PoolAllocator()
{
}



template<typename T>
/// <summary>
/// deallocates
/// </summary>
/// <param name="p">pointer to deallocate</param>
/// <param name="num">amount to deallocate</param>
inline void PoolAllocator<T>::deallocate(void* p, size_type num)
{

	MemoryManager* m = &MemoryManager::getInstance();

	m->freePool(p);
}





;

template<typename T>
template<class U, class ...Args>
/// <summary>
/// function to construct object 
/// </summary>
/// <param name="p">pointer</param>
/// <param name="...args">variadic argumentsd</param>
inline void PoolAllocator<T>::construct(U* p, Args&& ...args)
{
	new((void*)p) U(std::forward<Args>(args)...);
}
