#include "DrawList.h"

void DrawListItem::Render()
{
	glUseProgram(gi.program);
	glUniformMatrix4fv(1, 1, GL_FALSE,glm::value_ptr(gi.MVP));
	glBindVertexArray(gi.VAO);

	glDrawArrays(GL_TRIANGLES, 0, gi.vertLength);
	glBindVertexArray(0);
}
