#include "PoolAllocator.h"
#include "StackAllocator.h"
#include <unordered_map>
#pragma once
/// <summary>
/// mouse location struct
/// </summary>
struct MouseLocation 
{
	double x;
	double y;

};
/// <summary>
/// class representing the input manager
/// </summary>
class Input
{
public:
	//meyers singleton
	static Input& getInstance() 
	{
		static Input i;
		return i;
	}
	/// <summary>
	/// checks if key is pressed
	/// </summary>
	/// <param name="keycode">keycode of key</param>
	/// <returns>true/false</returns>
	bool Get(int keycode);
	/// <summary>
	/// sets teh state of gioven jey code
	/// </summary>
	/// <param name="keycode"></param>
	/// <param name="state"></param>
	void Set(int keycode, int state);
	/// <summary>
	/// gets the mouse location
	/// </summary>
	/// <returns>mopuse location pointer</returns>
	MouseLocation* GetMouseLocation();
	/// <summary>
	/// sets mouse location
	/// </summary>
	/// <param name="x">x</param>
	/// <param name="y">y</param>
	void SetMouseLocation(double x, double y);
	/// <summary>
	/// Get mouse button press
	/// </summary>
	/// <param name="buttonCode">button pressed</param>
	/// <returns>true/false</returns>
	bool GetMouseButton(int buttonCode);
	/// <summary>
	/// sets mouse button
	/// </summary>
	/// <param name="code">keycode</param>
	/// <param name="state">state of key</param>
	void SetMouseButton(int code, int state);
private:
	Input();
	Input(Input const&) = delete;
	void operator=(Input const&) = delete;
	/// <summary>
	/// keymap
	/// </summary>
	std::unordered_map<int, int, std::hash<int>, std::equal_to<int>, PoolAllocator<std::pair<const int, int>>> KeyMap;
	/// <summary>
	/// mouse map
	/// </summary>
	std::unordered_map<int, int, std::hash<int>, std::equal_to<int>, PoolAllocator<std::pair<const int, int>>> MouseMap;
	MouseLocation* ml;
};

