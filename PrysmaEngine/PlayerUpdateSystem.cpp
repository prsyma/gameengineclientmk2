#include "PlayerUpdateSystem.h"
Messenger* PlayerUpdateSystem::m = &Messenger::getInstance();
void PlayerUpdateSystem::Register(ECS::World* world)
{
	player = world->GetPlayer();
	box = m->GetBox(SYSTEMS::SCENE);
}

void PlayerUpdateSystem::Update(ECS::World* world)
{
	//could be limited to other than each frame etc./
	auto e = player->get<Transform>();
	auto e2 = player->get<Health>();
	PositionUpdate* posUp = alloc.allocate(1);
	alloc.construct(posUp);
	posUp->health = e2->health;
	posUp->position = e->postion;
	posUp->id = player->getEntityId();
	posUp->area = "";
	posUp->area = e->area;
	Message mes;
	mes.to = SYSTEMS::NETWORK;
	mes.from = SYSTEMS::SCENE;
	mes.data = (void*)posUp;
	mes.type = MessageType::POSITION;
	m->Send(mes.to, mes);
}

void PlayerUpdateSystem::Unregister(ECS::World* world)
{
}
