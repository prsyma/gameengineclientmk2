#include "Pool.h"
#include <stdexcept>
int Pool::blockSize = 0;
bool Pool::setup = false;
int Pool::numberOfBlocks = 0;
int Pool::freeBlocks = 0;
std::vector<Block>* Pool::blocks = &Pool::GetBlocks();
void* Pool::startOfPool = nullptr;
std::vector<MemoryWrapper>* Pool::wrappers = &Pool::GetWrappers();


Pool::Pool()
{
}


void Pool::DecrementCounter(int index)
{
	int val = (*wrappers)[index].counter.decrement();
	if (val <= 0)
	{
		deallocate(index);
	}
}

void Pool::IncrementCounter(int index)
{
	(*wrappers)[index].counter.increment();
}

int Pool::GetNumberOfFreeBlocks()
{
	return freeBlocks;
}

void* Pool::allocate(size_t size)
{
	//find the required number of blockes etc and mark them as used... 
	
	int reqBlocks = size / blockSize + 1;
	int startBlockid = -1;
	int currCount = 0;
	for (int i = 0; i < numberOfBlocks; i++)
	{
		if (currCount == reqBlocks)
		{
			break;
		}
		if ((*blocks)[i].used == true && currCount != reqBlocks)
		{
			
			startBlockid = -1;
			currCount = 0;
		}
		else
		{
			if (startBlockid == -1)
			{
				startBlockid = i;
			}
			currCount++;
		}

	}
	if (currCount != reqBlocks) 
	{
		throw new std::runtime_error("Not Enough Memory");
	}
	if (startBlockid == -1)
	{
		//throw error
		throw new std::runtime_error("Not Enough Memory");
	}
	else
	{
		//set to done
		freeBlocks -= reqBlocks;
		for (int i = 0; i < reqBlocks; i++)
		{
			(*blocks)[startBlockid + i].used = true;
		}
	}


	//find free wrapper

	for (int i = 0; i < numberOfBlocks; i++)
	{
		if (!(*wrappers)[i].Used())
		{
			(*wrappers)[i].location = (char*)startOfPool + (blockSize * startBlockid);
			(*wrappers)[i].size = size;
			return (*wrappers)[i].location;



		}
	}



	return nullptr;
}

void Pool::free(void* p)
{
	//we know location so we can caluclate the number of required blocks and the actual loccation
	int wrapperIndex = 0;
	for (int i =0; i < (*wrappers).size();i++)
	{
		if ((*wrappers)[i].location == p)
		{
			wrapperIndex = i;
			break;
		}
	}
	int reqBlocks = (*wrappers)[wrapperIndex].size / blockSize + 1;
	int startBlock = (int)((char*)(*wrappers)[wrapperIndex].location - (char*)startOfPool) / blockSize;
	(*wrappers)[wrapperIndex].location = nullptr;
	(*wrappers)[wrapperIndex].counter = PointerCounter();
	(*wrappers)[wrapperIndex].size = 0;
	for (int i = startBlock; i < reqBlocks; i++)
	{
		(*blocks)[i].used = false;
	}
}

int Pool::GetFreeMem()
{
	return blockSize*freeBlocks;
}

int Pool::totalMem()
{
	return numberOfBlocks*blockSize;
}

void Pool::SetUp(void* startOfPool, int sizeOfPool, int sizeOfBlocks)
{
	setup = true;
	blockSize = sizeOfBlocks;
	numberOfBlocks = sizeOfPool / sizeOfBlocks + 1;
	freeBlocks = numberOfBlocks;
	this->startOfPool = startOfPool;
	wrappers = &Pool::GetWrappers();
	blocks = &Pool::GetBlocks();

	(*wrappers).reserve(numberOfBlocks);
	(*blocks).reserve(numberOfBlocks);
	//push back maximum number of wrappers
	for (int i = 0; i < numberOfBlocks; i++)
	{
		(*wrappers).push_back(MemoryWrapper(i));
		(*blocks).push_back(Block());
	}
}

void Pool::defragment()
{
	int freeBlockCounter = 0;
	for (int i =0; i< numberOfBlocks;i++) 
	{
		if ((*blocks)[i].used = false)
		{
			freeBlockCounter++;
		}
		else if ((*blocks)[i].used == true && freeBlockCounter > 0)
		{
			//we should move this up
			//we need to find the matching wrapper
			for (int wrapperCounter = 0; wrapperCounter < numberOfBlocks; wrapperCounter++) 
			{
				if ((*wrappers)[wrapperCounter].location == ((char*)startOfPool + (blockSize * i)))
				{
					//we have found wrapper
					int bytesToMove = freeBlockCounter * blockSize;
					char* newPosition = (char*)(*wrappers)[wrapperCounter].location - bytesToMove;
					memmove(newPosition, (*wrappers)[wrapperCounter].location, (*wrappers)[wrapperCounter].size);
					(*wrappers)[wrapperCounter].updateLocation((void*)newPosition);
					//we now need to remove the blocks that have been free
					//we know the location so work backwards
					int blockLoc = (char*)startOfPool - newPosition;
					blockLoc = blockLoc / blockSize + 1;
					int blockIndex = blockLoc+((*wrappers)[wrapperCounter].size / blockSize + 1);
					for (int blockFreeing = blockIndex; blockFreeing < blockIndex+ freeBlockCounter; blockFreeing++)
					{
						(*blocks)[blockFreeing].used = false;
					}
					break;
				}
			}



			//move back by the free number of blocks and continue checking
			i -= freeBlockCounter;
			freeBlockCounter = 0;
		}
	}
}

void Pool::deallocate(int wrapperIndex)
{
	//we know location so we can caluclate the number of required blocks and the actual loccation
	int reqBlocks = (*wrappers)[wrapperIndex].size / blockSize + 1;
	int startBlock = (int)((char*)(*wrappers)[wrapperIndex].location - (char*)startOfPool)/blockSize;
	(*wrappers)[wrapperIndex].location = nullptr;
	(*wrappers)[wrapperIndex].counter = PointerCounter();
	(*wrappers)[wrapperIndex].size = 0;
	for (int i = startBlock; i < reqBlocks;i++)
	{
		(*blocks)[i].used = false;
	}
}


