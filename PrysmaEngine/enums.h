#include "Dependancies/GLFW/include/GLFW/glfw3.h"
#pragma once
/// <summary>
/// Enum Key Mapping
/// </summary>
enum KEYS 
{
	FORWARD = GLFW_KEY_W,
	LEFT = GLFW_KEY_A,
	RIGHT = GLFW_KEY_D,
	BACK = GLFW_KEY_S,
	JUMP = GLFW_KEY_SPACE
};
/// <summary>
/// Enum oif the system to retire their box
/// </summary>
enum SYSTEMS 
{
	SCENE,
	GRAPHIC,
	NETWORK,
	WINDOW
};