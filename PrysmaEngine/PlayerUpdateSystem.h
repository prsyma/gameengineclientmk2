#include "ECS.h"
#include "Messenger.h"
#include "PoolMessageAllocator.h"
#include "MessageTypes.h"
#include "Components.h"
#pragma once
class PlayerUpdateSystem : public ECS::System
{
public:
	PlayerUpdateSystem() {};

	void Register(ECS::World* world) override;

	void Update(ECS::World* world) override;

	void Unregister(ECS::World* world) override;

	~PlayerUpdateSystem() {};
private:
	ECS::Entity* player;
	float timeToSendUpdate = 0.1f;
	PoolMessageAllocator<PositionUpdate> alloc;
	static Messenger* m;
	#undef MessageBox
	MessageBox* box;
	#define MessageBox MessageBoxW
};

