#include "MemoryManager.h"
#include "StackAllocator.h"
#include "PoolMessageAllocator.h"
#include "MessageTypes.h"
#include "Messenger.h"
#include "enums.h"
#include "MessageBox.h"
#include <cpr/cpr.h>
#include <json.hpp>
#include <chrono>
#include <thread>
#include <future>
#include "ECS.h"
#include "Components.h"
#pragma once
using namespace std::chrono_literals;
class Network
{
public:
	Network();
	bool authenticate(std::string user, std::string pass, std::string connServer);
	int getId();
	void run(ECS::World* w);
	~Network();
	static int GetUpdates();
	static int checkMessages();
	void shutdown();
private:
	SYSTEMS system = SYSTEMS::NETWORK;
	//makes updates easier
	static ECS::World* world;
	//milli seconds
	int pollingSpeed = 200;
	int pollingSpeedGet = 200;
	static int id;
	static std::string connectionAddress;
	static cpr::Cookies cookies;
	static bool running;
	static bool authenicated;
	
	
	//static std::chrono::duration<double> sleepTime = 0.2s;
	

	
	static Messenger* m;
	static MemoryManager* mem;
	#undef MessageBox
	static MessageBox* box;
	//#define MessageBox MessageBoxW
};

