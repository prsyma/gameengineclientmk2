#include "Glad/glad.h"
#include "Dependancies/GLFW/include/GLFW/glfw3.h"
#include "MemoryManager.h"
#include "StackAllocator.h"
#include "MessageTypes.h"
#include "Messenger.h"
#include "enums.h"
#include "Input.h"
#include "DrawList.h"
#pragma once
/// <summary>
/// class for the window manager
/// </summary>
class WindowManager
{
public:
	/// <summary>
	/// cosntructor
	/// </summary>
	WindowManager();

	/// <summary>
	/// key call back function
	/// </summary>
	/// <param name="window">glfw window</param>
	/// <param name="key">key pressed</param>
	/// <param name="scancode">scan code</param>
	/// <param name="action">action</param>
	/// <param name="mods">modiifers</param>
	static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
	/// <summary>
	/// cursor position callback
	/// </summary>
	/// <param name="window">window</param>
	/// <param name="xpos">x</param>
	/// <param name="ypos">y</param>
	static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos);
	/// <summary>
	/// destructor
	/// </summary>
	~WindowManager();
	/// <summary>
	/// function to start
	/// </summary>
	void Start();
	/// <summary>
	/// shoudl the window close
	/// </summary>
	/// <returns>true/false</returns>
	bool ShouldClose();
	/// <summary>
	/// function to draw
	/// </summary>
	void Draw();
	/// <summary>
	/// function to stop
	/// </summary>
	void Stop();
private:
	
	void ReadConfig();
	void WriteConfig();
	
	int id;
	int w = 500;
	int h = 500;
	static Messenger* m;
	static MemoryManager* mem;
	static Input* input;
	GLFWwindow* window;
	#undef MessageBox;
	MessageBox* box;
	#define MessageBox MessageBoxW;
	
	
};

