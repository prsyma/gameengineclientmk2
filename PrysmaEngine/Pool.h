
#include "MemoryWrapper.h"
#include "SmartPointer.h"
#include <vector>
#pragma once
#ifndef BLOCK
#define BLOCK
struct Block
{

	bool used = false;

};
#endif // !BLOCK
/// <summary>
/// Class representinga  pool
/// </summary>
class Pool
{
public:
	//meyers singleton thread safe in c++11
	static Pool& getInstance()
	{
		static Pool m;
		return m;
	};
	/// <summary>
	/// Gets Blocks
	/// </summary>
	/// <returns>list of blocks</returns>
	static std::vector<Block>& GetBlocks() 
	{
		static std::vector<Block> blocks;
		return blocks;
	};
	/// <summary>
	/// Gets Wrappers
	/// </summary>
	/// <returns>vector of memory wrappers</returns>
	static std::vector<MemoryWrapper>& GetWrappers()
	{
		static std::vector<MemoryWrapper> wrappers;
		return wrappers;
	};
	/// <summary>
	/// Is teh pool setup
	/// </summary>
	/// <returns>bool of setup value</returns>
	bool IsSetUp()
	{
		return setup;
	}
	template<typename T, typename... Args>
	/// <summary>
	/// Allocates an  object using args for the constructor
	/// </summary>
	/// <param name="objectRequired">blank object required</param>
	/// <param name="...args">args for the constructor</param>
	/// <returns>smart pointer of correct type</returns>
	SmartPointer<T> allocate(T objectRequired, Args... args);
	void DecrementCounter(int index);
	void IncrementCounter(int index);
	//gets the number of free blocks
	int GetNumberOfFreeBlocks();
	/// <summary>
	/// 
	/// </summary>
	/// <param name="size"></param>
	/// <returns></returns>
	void* allocate(size_t size);
	/// <summary>
	/// Frees memory given void pointer
	/// </summary>
	/// <param name="p">start of memeory to free</param>
	void free(void* p);
	/// <summary>
	/// Gets teh free mem avaialble
	/// </summary>
	/// <returns>amount of memeory in bytes</returns>
	int GetFreeMem();
	/// <summary>
	/// gets total memory
	/// </summary>
	/// <returns>total amount of memeory in pool</returns>
	int totalMem();
	/// <summary>
	/// function to setup pool
	/// </summary>
	/// <param name="startOfPool">pointer to start of reserved memeory</param>
	/// <param name="sizeOfPool">size of memeory assigned to the pool</param>
	/// <param name="sizeOfBlocks">size of each block</param>
	void SetUp(void* startOfPool, int sizeOfPool, int sizeOfBlocks);
	/// <summary>
	/// destructor
	/// </summary>
	~Pool() {};
private:
	static int blockSize;
	static int numberOfBlocks;
	static int freeBlocks;
	static std::vector<MemoryWrapper>* wrappers;
	static bool setup;
	static std::vector<Block>* blocks;
	static void* startOfPool;
	Pool();
	Pool(void* startOfPool, int sizeOfPool, int sizeOfBlocks);
	Pool(Pool const&) = delete;
	void operator=(Pool const&) = delete;
	void defragment();
	void deallocate(int wrapperIndex);
	
};

template<typename T, typename ...Args>
/// <summary>
/// Function to allocate and return smart pointer
/// </summary>
/// <param name="objectRequired">required object type</param>
/// <param name="...args">varaidic arguments</param>
/// <returns>smart pointer of type T</returns>
inline SmartPointer<T> Pool::allocate(T objectRequired, Args ...args)
{

	//find the required number of blockes etc and mark them as used... 
	int reqBlocks = sizeof(T) / blockSize + 1;
	int startBlockid = -1;
	int currCount = 0;
	for (int i =0;i< numberOfBlocks;i++) 
	{
		if (currCount == reqBlocks) 
		{
			break;
		}
		if ((*blocks)[i].used == true && currCount != reqBlocks)
		{
			startBlockid = -1;
			currCount = 0;
		}
		else 
		{
			if (startBlockid == -1)
			{
				startBlockid = i;
			}
			currCount++;
		}
		
	}
	if (startBlockid == -1) 
	{
		//throw error
	}
	else 
	{
		//set to done
		freeBlocks -= reqBlocks;
		for (int i =0; i< reqBlocks;i++) 
		{
			(*blocks)[startBlockid + i].used = true;
		}
	}


	//find free wrapper
	
	for (int i = 0; i < numberOfBlocks;i++)
	{
		if (!(*wrappers)[i].Used()) 
		{
			(*wrappers)[i].location = startOfPool + (blockSize * startBlockid);
			(*wrappers)[i].size = sizeof(T);
			T* obj = new((*wrappers)[i].location) T(args...);
			SmartPointer<T> pointer((*wrappers)[i], 0);
			return pointer;
			

		}
	}
	
	
	
	
}
