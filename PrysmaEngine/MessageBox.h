#include "MessageList.h"
#include "MessageTypes.h"
#pragma once
/// <summary>
/// class representing a message box
/// </summary>
class MessageBox
{
public:
	/// <summary>
	/// constructor
	/// </summary>
	/// <param name="id">id of message box</param>
	MessageBox(int id);
	/// <summary>
	/// function to recieve a normal priority message
	/// </summary>
	/// <param name="message">message to receive</param>
	void RecieveNormal(Message message);
	/// <summary>
	/// function to recieve an isntant message
	/// </summary>
	/// <param name="message">message to receive</param>
	void RecieveInstant(Message message);
	/// <summary>
	/// function to check if list is sempy
	/// </summary>
	/// <returns></returns>
	bool NotEmpty();
	/// <summary>
	/// function to pop a message from regular queue
	/// </summary>
	/// <returns>mesaage</returns>
	Message pop();
	/// <summary>
	/// function to pop message from instant queue
	/// </summary>
	/// <returns>message</returns>
	Message popInstant();
private:
	int id;
	bool instantMessage = false;
	//have a FIFO list style
	MessageList normal;
	MessageList instant;
};

