#pragma once
#include "PointerCounter.h"

/// ********************************************************************************
/// <summary>
/// Wrapper for memeory class
/// </summary>
/// ********************************************************************************
class MemoryWrapper
{
public:
	MemoryWrapper() {};
	~MemoryWrapper() {};
	MemoryWrapper(const MemoryWrapper& wrapper);
	void operator=(MemoryWrapper const& wrapper);
	/// --------------------------------------------------------------------------------
	/// <summary>
	/// Constructor
	/// </summary>
	/// <param name="id">id of wrapper</param>
	/// <returns>void</returns>
	/// <created>Reece,08/06/2019</created>
	/// <changed>Reece,08/06/2019</changed>
	/// --------------------------------------------------------------------------------
	MemoryWrapper(int id);
	/// <summary>id number of wrappers</summary>
	int id;
	/// --------------------------------------------------------------------------------
	/// <summary>
	/// updates the location of the actual memeory
	/// </summary>
	/// <param name="loc">new location</param>
	/// <created>Reece,08/06/2019</created>
	/// <changed>Reece,08/06/2019</changed>
	/// --------------------------------------------------------------------------------
	void updateLocation(void* loc);

	void* getLocation();

	/// <summary>location of actual memeory</summary>
	void* location;
	/// --------------------------------------------------------------------------------
	/// <summary>
	/// Has the wrapper been used
	/// </summary>
	/// <returns>true if it has false otherwise</returns>
	/// <created>Reece,08/06/2019</created>
	/// <changed>Reece,08/06/2019</changed>
	/// --------------------------------------------------------------------------------
	bool Used();
	/// <summary>Counter for this wrapper</summary>
	PointerCounter counter;
	size_t size;
	bool markedForDealloc = false;

};

