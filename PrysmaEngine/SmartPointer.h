#include "MemoryWrapper.h"
#pragma once
template<typename T>
class SmartPointer
{
	SmartPointer(MemoryWrapper* wrapper, int location);
	/// <summary>
	/// destructor
	/// </summary>
	~SmartPointer();
	/// <summary>
	/// copy constructor
	/// </summary>
	/// <param name="pointerToCopy">pointer to copy</param>
	SmartPointer(const SmartPointer<T>& pointerToCopy);
	/// <summary>
	/// assignment operator
	/// </summary>
	/// <param name="pointerToAssign">pointer to assign</param>
	/// <returns>reference to smart pointer</returns>
	SmartPointer<T>& operator= (const SmartPointer<T>& pointerToAssign);
	/// <summary>
	/// equality operator
	/// </summary>
	/// <param name="pointer"></param>
	/// <returns></returns>
	bool operator==(SmartPointer<T>& pointer);
	/// <summary>
	/// deref operator
	/// </summary>
	/// <returns>reference to object</returns>
	T& operator *();
	/// <summary>
	/// returns pointer to object
	/// </summary>
	/// <returns></returns>
	T* operator ->();
private:
	int id;
	int dataLocation;
	int size;
	MemoryWrapper* wrapper;

};

