
#include "Pool.h"
#include "MessagePool.h"
#include "Stack.h"
#include <string>
#include <cstdlib>
#pragma once
enum MemoryType
{
	POOL,
	STACK,
	DB_STACK,
	MESSENGER,
	GRAPHICS
};

/// ********************************************************************************
/// <summary>
/// Class Representing the memeory manager
/// </summary>
/// ********************************************************************************
class MemoryManager
{
public:
	//Meyers singlton automatically thread safe in c++ 11
	static MemoryManager& getInstance()
	{
		static MemoryManager m;
		return m;
	}

	/// --------------------------------------------------------------------------------
	/// <summary>
	/// Function to get smart pointer of object
	/// </summary>
	/// <param name="c">Class to get</param>
	/// <param name="args">Varaidic arguments</param>
	/// <param name="subsystemID">susbsystem id</param>
	/// <param name="memoryType">wheer to storer it </param>
	/// <returns>smart pointer of type t</returns>
	/// <created>Reece,20/08/2019</created>
	/// <changed>Reece,20/08/2019</changed>
	/// --------------------------------------------------------------------------------
	template<typename T, typename... Args>
	SmartPointer<T> get(T c, Args... args, int subsystemID, MemoryType memoryType = MemoryType::POOL);



	/// --------------------------------------------------------------------------------
	/// <summary>
	/// Allocates to the pool
	/// </summary>
	/// <param name="size">size of required memeory</param>
	/// <returns>void pointer to start of memeory</returns>
	/// <created>Reece,20/08/2019</created>
	/// <changed>Reece,20/08/2019</changed>
	/// --------------------------------------------------------------------------------
	void* allocatePool(size_t size);

	/// --------------------------------------------------------------------------------
	/// <summary>
	/// Frees memeoryu from the pool
	/// </summary>
	/// <param name="ptr">to free</param>
	/// <created>Reece,20/08/2019</created>
	/// <changed>Reece,20/08/2019</changed>
	/// --------------------------------------------------------------------------------
	void freePool(void* ptr);
	/// --------------------------------------------------------------------------------
	/// <summary>
	/// Allocates to the message pool
	/// </summary>
	/// <param name="size">size of required memeory</param>
	/// <returns>void pointer to start of memeory</returns>
	/// <created>Reece,20/08/2019</created>
	/// <changed>Reece,20/08/2019</changed>
	/// --------------------------------------------------------------------------------
	void* allocateMessagePool(size_t size);
	/// --------------------------------------------------------------------------------
/// <summary>
/// Frees memeoryu from the message poool
/// </summary>
/// <param name="ptr">to free</param>
/// <created>Reece,20/08/2019</created>
/// <changed>Reece,20/08/2019</changed>
/// --------------------------------------------------------------------------------
	void freeMessagePool(void* ptr);

	/// --------------------------------------------------------------------------------
/// <summary>
/// Allocates to the stack
/// </summary>
/// <param name="size">size of required memeory</param>
/// <returns>void pointer to start of memeory</returns>
/// <created>Reece,20/08/2019</created>
/// <changed>Reece,20/08/2019</changed>
/// --------------------------------------------------------------------------------
	void* allocateStack(size_t size);
	/// --------------------------------------------------------------------------------
	/// <summary>
	/// Frees memeoryu from the stack
	/// </summary>
	/// <param name="ptr">to free</param>
	/// <created>Reece,20/08/2019</created>
	/// <changed>Reece,20/08/2019</changed>
	/// --------------------------------------------------------------------------------
	void freeStack(void* ptr);
	//pointer to the start of memeory
	void* startOfMemory;

	void DecrementCounter(MemoryType memType, int wrapperID);
	void IncrementCounter(MemoryType memType, int wrapperID);
	std::string readout();
	
	//bool release(void* loc);
	private:
		int id;
		//total amoutn go memeory
		int totalMem;
		//poitner tostart of emmeroy
		static void* memStart;
		
		//constructor private
		MemoryManager();
		//no copy constructor
		MemoryManager(MemoryManager const&) = delete;
		//no assignment
		void operator=(MemoryManager const&) = delete;
		//private destructor
		~MemoryManager();
		//have pool stack etc...
		static Pool* pool;
		static MessagePool* messagePool;
		static Stack* stack;
		bool setup = false;
	};
	/// --------------------------------------------------------------------------------
/// <summary>
/// Function to get smart pointer of object
/// </summary>
/// <param name="c">Class to get</param>
/// <param name="args">Varaidic arguments</param>
/// <param name="subsystemID">susbsystem id</param>
/// <param name="memoryType">wheer to storer it </param>
/// <returns>smart pointer of type t</returns>
/// <created>Reece,20/08/2019</created>
/// <changed>Reece,20/08/2019</changed>
/// --------------------------------------------------------------------------------
template<typename T, typename ...Args>
inline SmartPointer<T> MemoryManager::get(T c, Args ...args, int subsystemID, MemoryType memoryType)
{
	if (memoryType == MemoryType::POOL)
	{
		return pool.allocate(c, args);
	}
	else if (memoryType == MemoryType::STACK)
	{
		return stack.allocate(c, args);
	}
	else if (memoryType == MemoryType::GRAPHICS)
	{

	}
	return SmartPointer<T>();
}
