#include "UpdateSystem.h"
Messenger* UpdateSystem::m = &Messenger::getInstance();


UpdateSystem::UpdateSystem()
{
}

void UpdateSystem::Register(ECS::World * world)
{
	timeRemaining = timeToUpdate;
	box = m->GetBox(SYSTEMS::SCENE);
}

void UpdateSystem::Update(ECS::World * world)
{
	//we either do all of the messages or spend a limited amount of time
	while (true) 
	{
		Message mes = box->pop();
		if (mes.data == nullptr) 
		{
			break;
		}
		if (mes.type == MessageType::POSITION) 
		{
			//update 
			PositionUpdate* posUp = (PositionUpdate*)mes.data;
			if (posUp->id != world->GetPlayer()->getEntityId()) 
			{
				ECS::Entity* ent = world->getById(posUp->id);
				if (ent == nullptr) 
				{
					ent = world->create(posUp->id);
					auto entTrans = ent->assign<Transform>();
					entTrans->postion = posUp->position;
					auto b = ent->assign<Health>();
					b->health = posUp->health;
					auto r = ent->assign<Render>();
					r->gi = *world->playerModel;
				}
				else 
				{
					auto e = ent->get<Transform>();
					e->postion = posUp->position;
					auto b = ent->get<Health>();
					b->health = posUp->health;
				}
				
			}
		
			alloc.deallocate(mes.data, 1);
		}
		else 
		{
			break;
		}

		timeRemaining -= world->GetDelta();
		if (timeRemaining < 0.0f) 
		{
			timeRemaining = timeToUpdate;
			break;
		}
	}
}


void UpdateSystem::Unregister(ECS::World* world)
{
}

UpdateSystem::~UpdateSystem()
{
}
